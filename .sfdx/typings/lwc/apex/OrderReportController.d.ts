declare module "@salesforce/apex/OrderReportController.GetCustomSetting" {
  export default function GetCustomSetting(): Promise<any>;
}
declare module "@salesforce/apex/OrderReportController.PlaceandGetReportData" {
  export default function PlaceandGetReportData(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/OrderReportController.FetchPendingInquiry" {
  export default function FetchPendingInquiry(param: {inquiryId: any, recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/OrderReportController.GetUpdatedReportData" {
  export default function GetUpdatedReportData(param: {recordId: any}): Promise<any>;
}
