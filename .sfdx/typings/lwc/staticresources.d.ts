declare module "@salesforce/resourceUrl/CDStatic" {
    var CDStatic: string;
    export default CDStatic;
}
declare module "@salesforce/resourceUrl/CreditDeviceCustomSettingData" {
    var CreditDeviceCustomSettingData: string;
    export default CreditDeviceCustomSettingData;
}
declare module "@salesforce/resourceUrl/SLDS11" {
    var SLDS11: string;
    export default SLDS11;
}
