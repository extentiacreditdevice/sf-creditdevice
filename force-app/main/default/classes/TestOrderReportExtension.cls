@isTest
public class TestOrderReportExtension 
{
    static testmethod void TestOrderReport()
    {
        PermissionSet permissionName= new PermissionSet();
        permissionName.Name='Test';
        permissionName.Label='Test';
        // Add all required field 
        insert permissionName;
        Test.StartTest(); 
        ApexPages.currentPage().getParameters().put('0PS0o000002X3p3GAC','CreditDevice_Admin');
        ApexPages.StandardController sc = new ApexPages.StandardController(permissionName);
        OrderReportExtension checkPermission= new OrderReportExtension(sc);
        checkPermission.hasPermission=false;
        System.assertEquals(false,checkPermission.hasPermission);
        Test.StopTest();
    }
}