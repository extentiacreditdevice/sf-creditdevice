@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock 
{
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req)
    {
        // Optionally, only send a mock response for a specific endpoint
        HttpResponse res = new HttpResponse();
        if(req.getEndpoint().contains('companies'))
        {
            // Create a fake response
            String companyJson=        '{'+
                '    "list": ['+
                '        {'+
                '            "id": "l5eG0lBqUSPQshaAs4cioA",'+
                '            "name": "Test",'+
                '            "reg_number": "12345678",'+
                '            "est_number": "000012345678",'+
                '            "vat_number": null,'+
                '            "address": "Hoendiepstraat",'+
                '            "zipcode": "1079LS",'+
                '            "city": "Amsterdam",'+
                '            "country": "NL",'+
                '            "additional": "Hoofdvestiging"'+
                '        }'+
                '    ],'+
                '    "meta": {'+
                '        "items": 10,'+
                '        "current_page": 1,'+
                '        "more_items": true'+
                '    }'+
                '}';
            res.setBody(companyJson);
            res.setStatusCode(200);
        }
        else if(req.getEndpoint().contains('instants'))
        {
            String instantJson=       '{'+
                '  "disclaimer": "Test",'+
                '  "registration": {'+
                '  "dates": {'+
                '   "settlement":"2016-03-12"}'+
                '  },'+
                '"relations": {"branches": ['+
                '{'+
                '"name": "Rockwool B.V.",'+
                '"tradename": "Rockwool B.V.",'+
                '"reg_number": "130144280001",'+
                '"branch_number": "000021730873",'+
                '"address": "Delfstoffenweg 2",'+
                '"street": "Delfstoffenweg",'+
                '"house_number": "2",'+
                '"zipcode": "6045JH",'+
                '"city": "ROERMOND",'+
                '"country": "NL",'+
                '"company_active": true,'+
                '"creditdevice_id": "v1RevZTMfWWlrz7QZ/V2PA=="'+
            	'}]'+
                '},'+
                '  "contact_info": {'+
                '    "name": "Test B.V.",'+
                '    "tradenames": ['+
                '      "Test B.V."'+
                '    ],'+
                '    "address_office": {'+
                '      "address": "Test",'+
                '      "zipcode": "6045JG",'+
                '      "city": "Roermond",'+
                '      "country": "NL"'+
                '    },'+
                '    "address_postal": {'+
                '      "address": "Test",'+
                '      "zipcode": "6040KD",'+
                '      "city": "Roermond",'+
                '      "country": "NL"'+
                '    },'+
                '    "tel_number": "0475-123456",'+
                '    "fax_number": "1234567",'+
                '    "mail": "test@test.com",'+
                '    "website": "www.test.nl"'+
                '  }'+
                +'}';
            res.setBody(instantJson);
            res.setStatusCode(200);
        }
        else if(req.getEndpoint().contains('report'))
        {
            String reportJson = '{'+
                ' "disclaimer": "Test",'+
                ' "review": {'+
                '  "score": 7.1,'+
                ' "limit_advice": "EUR 1.000.000",'+
                '  "rating": "AAA",'+
                '  "risk": "Verlaagd risico",'+
                ' "specification": "Deze onderneming"'+
                ' },'+
                '"payments": {'+
                ' "score": 7.11'+
                ' }'+
                '}';
            res.setBody(reportJson);
            res.setStatusCode(200);
        }
        else if(req.getEndpoint().contains('inquiries'))
        {
            String inquiryJson = '{'+
                ' "item": {'+
                ' "id": "1",'+
                ' "name": "Test International B.V.",'+
                ' "address": "Postbus 86",'+
                ' "zipcode": "1000AA",'+
                ' "city": "Amsterdam",'+
                ' "country": "NL",'+
                ' "reg_number": "1234567HG",'+
                ' "vat_number": "12345678",'+
                ' "tel_number": null,'+
                ' "product": "D44CI701",'+
                ' "priority": "immediate",'+
                ' "language": "NL",'+
                ' "amount": 5647.5,'+
                ' "reference": "test",'+
                ' "reason": "Test",'+
                ' "status": "finished",'+
                ' "created_at": "2016-09-25T12:00:00Z",'+
                ' "finished_at": "2016-09-25T01:00:00Z"'+
                ' }'+    
                '}';
            res.setBody(inquiryJson);
            res.setStatusCode(200);
        }
        else if(req.getEndpoint().contains('monitor/alerts'))
        {
            String alertJson ='{"list": [';         
            for(Integer i=1;i<1000;i++)
            {
                alertJson += '{ "id": "1", "reg_number": "12345'+i+'", "reference": "", "type": "REVIEW_INCREASE", "created_at": "2015-09-26T00:00:00Z", "refs": { "self": "/api/monitor/alerts/1", "report": "/api/monitor/alerts/1/report" } },';
            }
            alertJson+= '{ "id": "1", "reg_number": "12345100", "reference": "", "type": "REVIEW_INCREASE", "created_at": "2015-09-26T00:00:00Z", "refs": { "self": "/api/monitor/alerts/1", "report": "/api/monitor/alerts/1/report" } }],"meta": {"items": 100,"current_page": 1,"more_items": false}}';
            res.setBody(alertJson);
            res.setStatusCode(200);
        }
        else if(req.getEndpoint().contains('monitor/entries/'))
        {
            String monitoringJson = '{ "item": { "name": "Test International B.V.", "reg_number": "12345678", "reference": "", "startdate": "2016-09-25", "enddate": "2017-09-24", "continue": true, "created_at": "2015-09-25T12:00:00Z", "updated_at": "2015-09-25T12:00:00Z", "refs": { "self": "/api/monitor/entries/12345678" } } }';
            res.setBody(monitoringJson);
            res.setStatusCode(200);
        }
        return res;
    }
}