@isTest
public class TestCreditDeviceConnectController {
    @testSetup static void setup() {
        Credit_Device_Custom_Setting__c creds = new Credit_Device_Custom_Setting__c();
        creds.Name = 'CreditDeviceCreds';
        creds.Username__c = 'Test';
        creds.Password__c ='Test@1234';
        creds.Endpoint_URL__c = 'https://inquiry.creditandcollection.nl/api/';
        insert creds;
        Credit_Device_General_Setting__c generalSetting = new Credit_Device_General_Setting__c();
        generalSetting.Name = 'GeneralSetting';
        generalSetting.Disable_Lead__c = false;
        generalSetting.Dutch_Country_Code__c = 'NL';
        generalSetting.Dutch_Language_Code__c ='NL';
        generalSetting.International_Product_Code__c = '701';
        generalSetting.National_Product_Code__c ='702';
        generalSetting.Non_Dutch_Language_Code__c ='EN';
        generalSetting.CreditDevice__Alert_Type_ALL__c ='INSOLVENCY,REVIEW_INCREASE,REVIEW_DECREASE,TERMINATION';
        generalSetting.CreditDevice__Alert_Type_HQ__c ='NAW';
        insert generalSetting;
        
        List<FieldMapping__c> fieldMappingList = new List<FieldMapping__c>();
        FieldMapping__c fieldmapping1 = new fieldMapping__c();
        fieldmapping1.Name ='Account-name';
        fieldmapping1.SF_Fields__c ='name';
        fieldmapping1.API_Fields__c = 'contact_info/name';
        fieldmapping1.Object__c = 'Account';
        fieldMappingList.add(fieldmapping1);
        
        FieldMapping__c fieldmapping11 = new fieldMapping__c();
        fieldmapping11.Name ='Account-numberofemployees';
        fieldmapping11.SF_Fields__c ='numberofemployees';
        fieldmapping11.API_Fields__c = 'contact_info/fax_number';
        fieldmapping11.Object__c = 'Account';
        fieldMappingList.add(fieldmapping11);
        
        FieldMapping__c fieldmapping2 = new fieldMapping__c();
        fieldmapping2.Name ='Account-Description';
        fieldmapping2.SF_Fields__c ='description';
        fieldmapping2.API_Fields__c = 'contact_info/tradenames[]';
        fieldmapping2.Object__c = 'Account';
        fieldMappingList.add(fieldmapping2);
        
        FieldMapping__c fieldmapping3 = new fieldMapping__c();
        fieldmapping3.Name ='Lead-lastname';
        fieldmapping3.SF_Fields__c ='lastname';
        fieldmapping3.API_Fields__c = 'contact_info/name';
        fieldmapping3.Object__c = 'Lead';
        fieldMappingList.add(fieldmapping3);
        
        FieldMapping__c fieldmapping4 = new fieldMapping__c();
        fieldmapping4.Name ='AccountND-name';
        fieldmapping4.CreditDevice__International__c=true;
        fieldmapping4.SF_Fields__c ='name';
        fieldmapping4.API_Fields__c = 'name';
        fieldmapping4.Object__c = 'Account';
        fieldMappingList.add(fieldmapping4);
        
        FieldMapping__c fieldmapping5 = new fieldMapping__c();
        fieldmapping5.Name ='LeadND-lastname';
        fieldmapping5.CreditDevice__International__c=true;
        fieldmapping5.SF_Fields__c ='lastname';
        fieldmapping5.API_Fields__c = 'name';
        fieldmapping5.Object__c = 'Lead';
        fieldMappingList.add(fieldmapping5);  
        insert fieldMappingList;
        
        List<CreditDevice__Country__c> countryList = new List<CreditDevice__Country__c>();
        CreditDevice__Country__c country1 = new CreditDevice__Country__c();
        country1.CreditDevice__Country_Code__c ='NL';
        country1.Name = 'Netherlands';
        countryList.add(country1);
        
        CreditDevice__Country__c country2 = new CreditDevice__Country__c();
        country2.CreditDevice__Country_Code__c ='IN';
        country2.Name = 'India';
        countryList.add(country2);
        insert countryList;
        
        Account acc = new Account();
        acc.Name= 'Test account';
        acc.BillingCity   = 'Amsterdam';
        acc.CreditDevice__Company_Id__c ='l5eG0lBqUSUTshaAs4cioA';
        acc.CreditDevice__Is_International__c=true;
        acc.BillingPostalCode  ='1000AA';
        acc.BillingCountry='US';
        acc.CreditDevice__Registration_Number__c= '1234567HG';
        acc.CreditDevice__Head_Quarter__c =true;
        insert acc;
        
        Lead ld = new Lead();
        ld.LastName = 'Test Lead11';
        ld.Company = 'Test Company';
        ld.Country = 'NL';
        ld.Registration_Number__c = '12345678';
        ld.Company_Id__c ='l5eG0lBqABCDshaAs4cioA';
        ld.CreditDevice__Head_Quarter__c =true;
        //ld.Is_International__c =true;
        ld.city = 'Amsterdam';
        ld.PostalCode ='1000AA';
        insert ld;
        
        Lead ldN = new Lead();
        ldN.LastName = 'Test Lead11';
        ldN.Company = 'Test Company';
        ldN.Country = 'US';
        ldN.Registration_Number__c = '87654321';
        ldN.Company_Id__c ='l5eG0lBqABCDshaAs4cioA';
        ldN.CreditDevice__Head_Quarter__c =true;
        ldN.Is_International__c =true;
        ldN.city = 'Perth';
        ldN.PostalCode ='';
        insert ldN;
    }
    public testMethod static void TestFetchSearchResult()
    {
        Lead ld = [SELECT id from Lead LIMIT 1];
        System.assertNotEquals(null,ld);
        PageReference pageRef = Page.CreditDeviceConnectPage;
        Test.setCurrentPage(pageRef);
        // put the lead id as a parameter
        ApexPages.currentPage().getParameters().put('id',ld.id);
        // Call your apex method here and you will get code coverage
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        CreditDeviceConnectController controller = new CreditDeviceConnectController();
        List<SelectOption> countryList = CreditDeviceConnectController.getCountries();
        System.assertEquals(2,countryList.size());
        PageReference pageRef1 = controller.FetchSearchResult();
        controller.recordObjectName='Lead';
        controller.DisplayDetails();
        controller.Search();
        Test.stopTest();
    }
    public testMethod static void TestEstablishConnection()
    {
        Lead ld = [SELECT id from Lead LIMIT 1];
        System.assertNotEquals(null,ld);
        PageReference pageRef = Page.CreditDeviceConnectPage;
        Test.setCurrentPage(pageRef);
        // put the lead id as a parameter
        ApexPages.currentPage().getParameters().put('id',ld.id);
        // Call your apex method here and you will get code coverage
        CreditDeviceConnectController controller = new CreditDeviceConnectController();
        controller.selectedRecordRegNo = '12345678';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        PageReference pageRef1 = controller.FetchSearchResult();
        pageRef1 =controller.actCancel();
        pageRef1 =controller.UpdateRecord();
        controller.DisplayDetails();
        controller.postalCode='';
        controller.city='';
        controller.Search();
        pageRef1 = controller.EstablishConnection();
        Test.stopTest();
    }
    public testMethod static void TestNotblishConnection()
    {
        Lead ldn = [SELECT id from Lead LIMIT 1];
        System.assertNotEquals(null,ldn);
        Account acc=[SELECT id from Account LIMIT 1];
        System.assertNotEquals(null,acc);
        List<Account> accList =[ Select Id from account Limit 1];
        System.assertEquals(1,accList.size());
        PageReference pageRef = Page.CreditDeviceConnectPage;
        Test.setCurrentPage(pageRef);
        // put the lead id as a parameter
        ApexPages.currentPage().getParameters().put('id',acc.id);
        // Call your apex method here and you will get code coverage
        CreditDeviceConnectController controller = new CreditDeviceConnectController();
        controller.selectedRecordRegNo = '1234567HG';
        controller.SelectedItemNo= 1;
        controller.SelectedRecordId = 'l5eG0lBqABCDshaAs4cioA';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        PageReference pageRef1 = controller.FetchSearchResult();
        controller.recordObjectName= 'Account';
        controller.Search();
        controller.DisplayDetails();
        pageRef1 =controller.actCancel();
        pageRef1 =controller.UpdateRecord();
        pageRef1 = controller.EstablishConnection();
        Test.stopTest();
    }
    
    public static testMethod void TestConnectViewWrapper() {
        Boolean isChecked =false;
        String fieldAPIname ='Name';
        String fieldlabel ='Account Name';
        String fieldSFdata='Test B.V.';
        Object apiData = 'Test B.V.';
        
        CreditDeviceConnectController.ConnectViewWrapper connectWrap = new CreditDeviceConnectController.ConnectViewWrapper(isChecked, fieldAPIname, fieldlabel, fieldSFdata, apiData);
        System.assertNotEquals(null,connectWrap);
        //Covering inner/wrapper class
    }
}