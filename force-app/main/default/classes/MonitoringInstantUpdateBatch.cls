/***************************************************************
Class : MonitoringInstantUpdateBatch  
Developer : Sneha U.
Created Date : 03/26/19
Summary : This class is used to update Accounts/Leads which have alerts (Created with "without sharing" as need to update all accounts irrespective of the record access and also this batch will be executed by the system admin.)
Change Log: 
v1.0 - 03/26/19 - Sneha U. - Initial Creation.(100% Code Coverage)
v1.1 - 05/24/19 - Sneha U. - Include company is head quarter or branch and map accordingly
v1.2 - 06/12/19 - Kalpana G. - Add disable Lead functionality check
v1.2 - 07/29/19 - Sneha U. - Batch on Alerts instead of Account/Lead
*****************************************************************/
global without sharing class MonitoringInstantUpdateBatch  implements Database.Batchable<sObject>,Database.Stateful, Database.AllowsCallouts
{
    global final String query;
    global final String objectName;
    global Integer failedRecordCount = 0;    
    global Integer successRecordCount = 0;    
    global Integer totalRecordCount = 0;   
    global MonitoringInstantUpdateBatch  (String objName)
    {
        objectName = objName;
        //Get all alerts   
        query='select id,CreditDevice__'+objectName+'_Id__c,CreditDevice__Error__c,CreditDevice__Credit_Limit_Change__c from CreditDevice__Alert__c where CreditDevice__'+objectName+'_Id__c!=null'; 
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        try
        {  
            String recordIds ='(';
            Set<Id> setCreditLimitChange = new Set<Id>();
            List<SObject> listCompanyToUpdate =new List<SObject>();
            Map<Id,CreditDevice__Alert__c> mapCompanyIdAlert = new Map<Id,CreditDevice__Alert__c>();
            List<CreditDevice__Alert__c> listAlertsToUpdate = new List<CreditDevice__Alert__c>();

            //Get ids from all alert records
            for(SObject objAlert: scope){
                recordIds+='\''+objAlert.get('CreditDevice__'+objectName+'_Id__c').toString()+'\',';
                if(objAlert.get('CreditDevice__Credit_Limit_Change__c')!=null && Boolean.valueOf(objAlert.get('CreditDevice__Credit_Limit_Change__c')) ){
                    setCreditLimitChange.add(objAlert.get('CreditDevice__'+objectName+'_Id__c').toString());
                }
                mapCompanyIdAlert.put(objAlert.get('CreditDevice__'+objectName+'_Id__c').toString(),(CreditDevice__Alert__c)objAlert);
            }         
            recordIds = recordIds.removeEnd(',');
            recordIds+=')';

            List<SObject> listCompany = Database.query('select id,CreditDevice__Registration_Number__c,CreditDevice__Head_Quarter__c,CreditDevice__Est_Number__c from '+objectName+' where CreditDevice__Company_Id__c!=null and CreditDevice__Is_International__c=false and CreditDevice__Registration_Number__c!=null and id in '+recordIds );
            for(SObject objCompanyRecord: listCompany){

                //v1.1 - get the value of head quarter
                Boolean isHeadQuarter =false;
                if(objCompanyRecord.get('CreditDevice__Head_Quarter__c')!=null)
                {
                    isHeadQuarter = Boolean.valueOf(objCompanyRecord.get('CreditDevice__Head_Quarter__c'));
                }
                String currentESTNumber =''; 
                if(objCompanyRecord.get('CreditDevice__Est_Number__c')!=null)
                {
                    currentESTNumber = String.valueOf(objCompanyRecord.get('CreditDevice__Est_Number__c')) ;
                }
                //v1.1 end
                //Call utility method to populate instant report details
                SObject objCompany = MonitoringUtility.monitorWithInstantReport(isHeadQuarter,currentESTNumber, String.valueOf(objCompanyRecord.get('CreditDevice__Registration_Number__c') ),objectName);  
                if(objCompany!=null){
                    //Get the constructed record and add id to it
                    objCompany.Id = objCompanyRecord.Id;

                    //Set creit limit change on account
                    if(setCreditLimitChange.contains(objCompany.Id)){
                        objCompany.put('CreditDevice__Credit_Limit_Change__c',true);
                    }
                    listCompanyToUpdate.add(objCompany);
                }
            }           
            //Check if records exist
            if(listCompanyToUpdate.size()>0){

                //Update Account/Lead records
                List<Database.SaveResult> listUpdatedRecords = database.update(listCompanyToUpdate,false);
                for(Integer i=0;i<listUpdatedRecords.size();i++)
                {
                    Database.SaveResult sr = listUpdatedRecords[i];
                    if (!sr.isSuccess()) 
                    {
                        failedRecordCount+=1;
                        
                        //get the company id
                        String companyId = listCompanyToUpdate[i].get('id').toString();
                        if(mapCompanyIdAlert.containsKey(companyId ) && sr.getErrors().size()>0){
                            CreditDevice__Alert__c objAlert= mapCompanyIdAlert.get(companyId);
                            objAlert.CreditDevice__Error__c=sr.getErrors().get(0).getMessage();
                            listAlertsToUpdate.add(objAlert);
                        }
                    } 
                    else
                    {
                        successRecordCount+=1;
                    }                   
                }
            }
            totalRecordCount += listCompanyToUpdate.size();

            if(listAlertsToUpdate.size()>0){
                update listAlertsToUpdate;
            }
        }
        catch(Exception e)
        {
            AsyncApexJob asyn = [SELECT Id, Status, CreatedBy.Email, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE Id=:BC.getJobId() limit 1];
            String subject = objectName + ' Monitoring Instant Update Batch Report';
            Util.SendEmailToAdminUser(asyn,e.getMessage(),subject);   
        }
    }
    global void finish(Database.BatchableContext BC){
        Credit_Device_General_Setting__c generalSetting  = Credit_Device_General_Setting__c.getValues('GeneralSetting');
        Boolean isLeadDisabled = false;
        Integer batchsize = 10;
        Boolean isEmailNotificationDisabled=false;

        if(generalSetting!=null)
        {
            isLeadDisabled = generalSetting.CreditDevice__Disable_Lead__c;
            isEmailNotificationDisabled = generalSetting.Email_Notification_Disabled__c;

            //Get the batchSize
            if(generalSetting.CreditDevice__Monitoring_Batch_Size__c!=null && generalSetting.CreditDevice__Monitoring_Batch_Size__c!=0){
                batchsize=Integer.valueOf(generalSetting.CreditDevice__Monitoring_Batch_Size__c);
            }
        }
        //Execute for Leads once Account batch is completed
        if(objectName.equalsIgnoreCase('Account') && !isLeadDisabled)
        {
            //Assign null to get fields specific to Lead
            MonitoringUtility.MapSfFieldsToApiField=null;
            MonitoringUtility.MapofObjectFields=null;

            if(!Test.isRunningTest()){
                //Execute batch for Lead
                Database.executeBatch(new MonitoringInstantUpdateBatch('Lead'),batchsize);
            }
        }

        if(!isEmailNotificationDisabled)
        {
            AsyncApexJob asyn = [SELECT Id, Status, CreatedBy.Email,CreatedBy.Name, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE Id=:BC.getJobId() limit 1];
            String subject = objectName + ' Monitoring Instant Update Batch Report';
            String emailBody = 'Hi '+asyn.CreatedBy.Name + '\n\n';
            emailBody+= 'The batch apex job '+ asyn.Id+' is processed. \n\n';
            emailBody+= 'Batch Status: ' + asyn.Status+'\n';
            emailBody+= 'No. of records processed: '+successRecordCount + '\n';
            emailBody+= 'No. of records failed: '+failedRecordCount;
            Util.SendEmailToAdminUser(asyn,emailBody,subject);
        }
    }
}