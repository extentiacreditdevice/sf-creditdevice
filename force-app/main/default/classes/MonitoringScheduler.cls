/***************************************************************
Class : MonitoringScheduler 
Developer : Sneha U.
Created Date : 04/1/19
Summary : This class is used to schedule monitoring entries for accounts & leads
Change Log: 
v1.0 - 04/1/19 - Sneha U. - Initial Creation.(100% Code Coverage)
*****************************************************************/
global class MonitoringScheduler implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
            //Get the batch size
            Credit_Device_General_Setting__c generalSetting = Credit_Device_General_Setting__c.getValues('GeneralSetting');
            
            Integer batchsize = 10;
            if(generalSetting!=null && generalSetting.CreditDevice__Monitoring_Batch_Size__c!=null && generalSetting.CreditDevice__Monitoring_Batch_Size__c!=0){
                batchsize=Integer.valueOf(generalSetting.CreditDevice__Monitoring_Batch_Size__c);
            }
            Database.executeBatch(new MonitoringBatch('Account'),batchsize);

    }
}