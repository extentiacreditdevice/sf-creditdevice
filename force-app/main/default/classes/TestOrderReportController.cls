@isTest
public class TestOrderReportController {

    @testSetup static void setup() {
        Lead ld = new Lead();
        ld.LastName = 'Test Lead';
        ld.Company = 'Test Company';
        ld.Country = 'NL';
        ld.Registration_Number__c = '1234567HG';
        ld.Company_Id__c ='354768GABCD456378';
        ld.Is_International__c =true;
        ld.city = 'Amsterdam';
        ld.PostalCode ='1000AA';
        insert ld;
        
        Credit_Device_Custom_Setting__c creds = new Credit_Device_Custom_Setting__c();
        creds.Name ='CreditDeviceCreds';
         creds.Username__c = 'Test';
         creds.Password__c = 'Test@1234';
        creds.Endpoint_URL__c = 'https://inquiry.creditandcollection.nl/api/';
        insert creds;
        
        Credit_Device_General_Setting__c generalSetting = new Credit_Device_General_Setting__c();
        generalSetting.Name = 'GeneralSetting';
        generalSetting.Disable_Lead__c = false;
        generalSetting.Dutch_Country_Code__c = 'NL';
        generalSetting.Dutch_Language_Code__c ='NL';
        generalSetting.International_Product_Code__c = '701';
        generalSetting.National_Product_Code__c ='702';
        generalSetting.Non_Dutch_Language_Code__c ='EN';
        generalSetting.CreditDevice__Alert_Type_ALL__c ='INSOLVENCY,REVIEW_INCREASE,REVIEW_DECREASE,TERMINATION';
        generalSetting.CreditDevice__Alert_Type_HQ__c ='NAW';
        insert generalSetting;
         
    }
    public static testMethod void TestPlaceandGetReportData()
    {
        Lead ld = [Select Id from Lead LIMIT 1];
        System.AssertNotEquals(null,ld);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        String response = OrderReportController.PlaceandGetReportData(ld.id);
        System.AssertNotEquals(null,response);
        Test.stopTest();
    }
    public static testMethod void TestFetchPendingInquiry()
    {
        Lead ld = [Select Id from Lead LIMIT 1];
        System.AssertNotEquals(null,ld);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        APIResponse getchedResponse = OrderReportController.FetchPendingInquiry('1',String.valueOf(ld.id));
        Test.stopTest();
    }
    public static testMethod void TestGetUpdatedReportData()
    {
        Lead ld = [Select Id from Lead LIMIT 1];
        System.AssertNotEquals(null,ld);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        APIResponse getchedResponse = OrderReportController.GetUpdatedReportData(ld.id);
        Test.stopTest();
    }
    public static testMethod void TestAddErrorMessage()
    {
        APIResponse response = new APIResponse();
        String error = 'Test Error';
        response.AddErrorMessage(error);
        System.AssertNotEquals(0,response.MessageList.size());
        response.AddErrorMessages(new List<String>{error});
    }
    public static testMethod void TestgereralSetting()
    {
        APIResponse response = OrderReportController.GetCustomSetting();
        System.AssertNotEquals(null,response);
    }
}