/***************************************************************
    Class : APIResponse  
    Developer : HetalS
    Created Date : 03/14/19
    Summary : This class is used to return the api response detail
    Change Log: 
    v1.0 - 03/14/19 - HetalS - Created this new controller.(100% Code Coverage)
    
   *****************************************************************/
public with sharing class APIResponse  {    

    @AuraEnabled public Boolean Success {get; set;}
    @AuraEnabled public List<Map<String, Object>> MessageList {get; set;}
    @AuraEnabled public Map<String, Object> DataMap {get; set;}
    private final string TYPE_ERROR = 'ERROR';
    // Constructors
    public APIResponse()
    {
        this.Success = true;
        this.MessageList = new List<Map<String, Object>>();
        this.DataMap = new Map<String, Object>();
    }
/*
* Description: This method is used to add single error message on the screen.
*/
    public void AddErrorMessage(String message)
    {
        AddMessage(TYPE_ERROR, message);
    }
/*
* Description: This method is used to add multiple error messages on the screen.
*/
    public void AddErrorMessages(List<String> messages)
    {
        AddMessages(TYPE_ERROR, messages);
    }
/*
* Description: This method is used to display multiple error message on the screen.
*/
    public void AddMessages(String typeKey, List<String> messagesToAdd)
    {
        if (messagesToAdd == null) return;
        for (string messageToAdd : messagesToAdd) 
        {
            AddMessage(typeKey, messageToAdd);
        }
    }
/*
* Description: This method is used to display single error message on the screen.
*/
    public void AddMessage(String typeKey, string messageToAdd)
    {
        Map<String, Object> messageMap = new Map<String, Object>();
        messageMap.put('type', typeKey);
        messageMap.put('message', messageToAdd);
        this.MessageList.add(messageMap);
    }
}