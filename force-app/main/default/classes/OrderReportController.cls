/***************************************************************
Class : OrderReportController
Developer : HetalS
Created Date : 03/14/19
Summary : This class is used to place an inquiry and fetch report from lightning component
Change Log: 
v1.0 - 03/14/19 - HetalS - Created this new controller.(86% Code Coverage)
v1.0 - 03/15/19 - HetalS - Set the countrycode as NL for Dutch company and none for Non-Dutch to place the inquiry
v1.0 - 07/08/19 - Sneha U. - Account/Lead field on Inquiry object check made conditional to make sure Standard Platform User can order report
*****************************************************************/
public with sharing class OrderReportController 
{
    /* Description: This method is used to get the custom setting
*/    
    @auraEnabled
    public static APIResponse GetCustomSetting()
    {        
        APIResponse response = new APIResponse();
        try
        {
            Credit_Device_General_Setting__c generalSetting  = Credit_Device_General_Setting__c.getValues('GeneralSetting');   
            response.DataMap.put('settings',generalSetting);
        }
        catch(Exception e)
        {
            response.Success = false;
            response.AddErrorMessage(e.getMessage());
        } 
        return response;  
    }
    /*
* Description: This method is used to place inquiry and get the credit report detail based on the record id passed.
* Param: recordId = Account/Lead  id 
*/
    @auraEnabled
    public static String PlaceandGetReportData(Id recordId)
    {
        APIResponse response = new APIResponse();
        InquiryResponseWrapper inquiryResponseObj = new InquiryResponseWrapper();
        try
        {          
            String recordObjectName = recordId.getSObjectType().getDescribe().getName();
            HttpResponse inquiryResponse = PlaceInquiry(recordId, recordObjectName);   
  
            if (inquiryResponse.getStatusCode() == 200) 
            {
                inquiryResponseObj = (InquiryResponseWrapper) System.JSON.deserialize(inquiryResponse.getBody(), InquiryResponseWrapper.class);                      
                String currentInquiryId = (inquiryResponseObj != null && inquiryResponseObj.item != null) ? inquiryResponseObj.item.id : null;
                String status = (inquiryResponseObj != null && inquiryResponseObj.item != null) ? inquiryResponseObj.item.status : 'pending';
                if('finished'.EqualsIgnoreCase(status))
                {
                    response = GetAndUpdateReportData(currentInquiryId, recordId);
                }     
                CreateInquiryRecord(inquiryResponseObj,inquiryResponseObj.item.id, recordId, recordObjectName) ;         
                response.DataMap.put('inquiryResponse',inquiryResponseObj);
            }
            else
            {
                response.Success = false;
                response.AddErrorMessage(inquiryResponse.getStatus());
            }          
        }
        catch(Exception e)
        {          
            response.Success = false;
            response.AddErrorMessage(e.getMessage());            
        }        
        return System.JSON.serialize(response);
    }
    /*
* Description: This method is used to place inquiry 
* Param: recordId = Account/Lead  id 
*/
    public static HttpResponse PlaceInquiry(Id recordId, String recordObjectName)
    {
        Http httpInquiry = new Http();
        HttpResponse inquiryResponse = httpInquiry.send(CreateInquiryRequest(recordId, recordObjectName));    
        return inquiryResponse;
    }
    /*
* Description: This method is used to generate the inquiry request. 
* Param: recordId = Account/Lead  id 
*/
    public static HttpRequest CreateInquiryRequest(Id recordId, String recordObjectName)
    {
        Credit_Device_General_Setting__c generalSetting = Credit_Device_General_Setting__c.getValues('GeneralSetting');
        HttpRequest request = new HttpRequest();        
        Map<String,String> mapRequestHeader = new Map<String,String>();
        mapRequestHeader.put('Content-Type','application/json');
        SObject currentRecord;            
        String companyId;        
        InquiryWrapper requestWrapperObj;
        String recordFields;         
        String regNumber;
        String vatNumber;
        String countryCode = 'NL'; //Default to Netherland
        String language = 'NL'; //Default to Dutch language
        String productCode = 'D44CI702';  //Default to national product code    
        if('Account'.EqualsIgnoreCase(RecordObjectName))
        {
            recordFields= 'Id,Name,Company_Id__c,Is_International__c,Registration_Number__c,Registration_VAT_Number__c,BillingCity,BillingCountry,BillingPostalCode,ShippingCity,ShippingCountry,ShippingPostalCode'; 
        }
        else
        {
            recordFields= 'Id,Company_Id__c,Is_International__c,Registration_Number__c,Registration_VAT_Number__c,city,Country,PostalCode'; 
        }
        String whereCondition = 'Id =: recordId ';
        String query = String.format('SELECT {0}  ' + 'FROM {1} ' +  'WHERE {2} ' , new List<String>{recordFields, recordObjectName , whereCondition});  
        List<SObject> listRecord = (List<SObject>)Database.query(query);  
        if(listRecord != null && !listRecord.isEmpty())
        {
            currentRecord = listRecord[0];
        }  
        if(generalSetting != null)
        {
            countryCode = generalSetting.Dutch_Country_Code__c;
            language = generalSetting.Dutch_Language_Code__c;
            productCode = generalSetting.National_Product_Code__c;
            if(Boolean.valueOf(currentRecord.get('Is_International__c')))
            {    
                countryCode = null;         
                language = generalSetting.Non_Dutch_Language_Code__c; 
                productCode = generalSetting.International_Product_Code__c;
                companyId  = String.valueOf(currentRecord.get('Company_Id__c'));             
            }  
        }     
        regNumber = (currentRecord.get('Registration_Number__c') != null ? String.valueOf(currentRecord.get('Registration_Number__c')) : null);
        vatNumber = (currentRecord.get('Registration_VAT_Number__c') != null ? String.valueOf(currentRecord.get('Registration_VAT_Number__c')) : null);
        requestWrapperObj = new InquiryWrapper(productCode,countryCode,regNumber,language,'immediate',companyId,vatNumber);                        
        String requestBody = JSON.serialize(requestWrapperObj,true);  
        request = Util.GetGeneratedRequest(mapRequestHeader,'inquiries',requestBody ,'POST');
        request.setTimeout(120000);        
        return request;
    }  
    /*
* Description: This method is used to hit credit report api and get the report data based on the inquiry generated
* Param: recordId = Account/Lead  id 
* Param: currentInquiryId = Inquiry Id
*/
    public static APIResponse GetAndUpdateReportData(String currentInquiryId, Id recordId)
    {
        APIResponse response = new APIResponse();
        try
        {
            Http httpReport = new Http();                
            HttpResponse reportResponse = httpReport.send(CreateCreditReportRequest(currentInquiryId));
            if (reportResponse.getStatusCode() == 200) 
            {
                CreditReportResponseWrapper reportResponseObj = (CreditReportResponseWrapper) System.JSON.deserialize(reportResponse.getBody(), CreditReportResponseWrapper.class);
                String recordObjectName = recordId.getSObjectType().getDescribe().getName();
                UpdateAccountORLeadWithReportsData(reportResponseObj,recordId, recordObjectName ,currentInquiryId);  
                UpdateInquiry(new set<string>{currentInquiryId});
            }     
            else
            {
                response.Success = false;
                response.AddErrorMessage(reportResponse.getStatus());           
            }
        }
        catch(Exception e)
        {
            response.Success = false;
            response.AddErrorMessage(e.getMessage());
        }        
        return response;
    }
    /*
* Description: This method is used to update specific fields on Account/Lead based on the credit report response
*/   
    public static void UpdateAccountORLeadWithReportsData(CreditReportResponseWrapper reportResponseObj, Id recordId, String ObjectName, String currentInquiryId)
    {  
        try
        {   
            List<String> listSObjectFieldNames = new List<String>{'id', 'CreditDevice__Credit_Rating__c','CreditDevice__Score__c','CreditDevice__Limit_Advice__c','CreditDevice__Payment_Score__c','CreditDevice__Risk__c','CreditDevice__Credit_Report_Order_Date__c','CreditDevice__InquiryId__c'};
                if('Account'.EqualsIgnoreCase(ObjectName))
            {
                Util.checkPermissions(new Account(),'Update',listSObjectFieldNames);     
            }
            else
            {
                Util.checkPermissions(new Lead(),'Update',listSObjectFieldNames);  
            }               
            SObject recordObj = Schema.getGlobalDescribe().get(ObjectName).newSObject() ;            
            recordObj.put('id',recordId);
            recordObj.put('Credit_Rating__c',(reportResponseObj.review != null ? reportResponseObj.review.rating : ''));        
            recordObj.put('Score__c',(reportResponseObj.review != null ? reportResponseObj.review.score : null));
            recordObj.put('Limit_Advice__c',(reportResponseObj.review != null ? reportResponseObj.review.limit_advice : ''));
            recordObj.put('Payment_Score__c',(reportResponseObj.payments != null ? reportResponseObj.payments.score : null));
            recordObj.put('Risk__c',(reportResponseObj.review != null ? reportResponseObj.review.risk : ''));
            recordObj.put('Credit_Report_Order_Date__c',Date.today());
            recordObj.put('InquiryId__c',CurrentInquiryId);             
            update recordObj; 
        }
        catch(Exception e)
        {
            throw e;
        } 
    }
    /*
* Description: This method is used to update the inquiry record once the credit report api is successful
*/
    @future
    public static void UpdateInquiry(set<string> setInquiryId)
    {
        List<Inquiry__c> listInquiry = [SELECT Id,Inquiry_ID__c, Status_code__c FROM Inquiry__c WHERE Inquiry_ID__c IN :setInquiryId];
        if(listInquiry != null)
        {
            for(Inquiry__c item: listInquiry)
            {
                item.Status_code__c = 'finished';
            }
            try
            {                
                update listInquiry;
            }
            catch(DMLException dx)
            {
                system.debug('Error: An exception has been encountered while updating inquiry record :'+dx.getDmlMessage(0));
            }
            catch(Exception e){
                system.debug('Error: An exception has been encountered while updating inquiry record :'+e.getMessage());
            }
        }       
    }
    /*
* Description: This method is used to fetch the pending inquiry 
*/
    @auraEnabled
    public static APIResponse FetchPendingInquiry(String inquiryId, string recordId){
        APIResponse response = new APIResponse();
        try
        {            
            InquiryResponseWrapper inquiryResponseObj;
            HttpResponse inquiryResponse  = FetchInquiry(inquiryId);
            if (inquiryResponse.getStatusCode() == 200) 
            {
                inquiryResponseObj = (InquiryResponseWrapper) System.JSON.deserialize(inquiryResponse.getBody(), InquiryResponseWrapper.class);
                String status = (inquiryResponseObj != null && inquiryResponseObj.item != null) ? inquiryResponseObj.item.status : 'pending';
                if('finished'.EqualsIgnoreCase(status))
                {
                    response = GetAndUpdateReportData(inquiryId, recordId);  
                }
                response.DataMap.put('inquiryResponse',inquiryResponseObj);   
            }
            else
            {
                response.Success = false;
                response.AddErrorMessage(inquiryResponse.getStatus());
            }                             
        }
        catch(Exception e)
        {
            response.Success = false;
            response.AddErrorMessage(e.getMessage());
        }
        return response;        
    }
    /*
* Description: This method is used to get the updated report fields to be displayed on detail page component
*/    
    @auraEnabled
    public static APIResponse GetUpdatedReportData(Id recordId){
        APIResponse response = new APIResponse();
        try
        {
            Map<String,Object> dataMap = new Map<String,Object>();
            String recordObjectName = recordId.getSObjectType().getDescribe().getName();
            String recordFields= 'Id,Name,Credit_Limit_Change__c, Credit_Rating__c,Report__c,Credit_Report_Order_Date__c,Limit_Advice__c,Payment_Score__c,Risk__c,Score__c'; 
            String whereCondition = 'Id =: recordId ';
            String query = String.format('SELECT {0}  '+'FROM {1} '+'WHERE {2} '+' ORDER BY LastModifiedDate DESC ', new List<String>{recordFields, recordObjectName, whereCondition});  
            List<SObject> listRecord = (List<SObject>)Database.query(query);        
            if(listRecord != null)
            {
                for(SObject sObj:listRecord )
                {
                    sObj.put('Credit_Limit_Change__c', false);
                    dataMap.put('creditRating',sObj.get('Credit_Rating__c'));
                    dataMap.put('reportLink',sObj.get('Report__c'));
                    dataMap.put('orderDate',sObj.get('Credit_Report_Order_Date__c'));
                    dataMap.put('creditLimit',sObj.get('Limit_Advice__c'));
                    dataMap.put('paymentScore',sObj.get('Payment_Score__c'));
                    dataMap.put('risk',sObj.get('Risk__c'));
                    dataMap.put('score',sObj.get('Score__c'));
                    dataMap.put('hasCreditLimitChange',sObj.get('Credit_Limit_Change__c')); 
                }
                update listRecord;
            }
            response.DataMap.put('data',dataMap);    
        }
        catch(Exception e)
        {
            response.Success = false;
            response.AddErrorMessage(e.getMessage());
        }            
        return response;
    }   
    /*
* Description: This method is used to create the new inquiry record based on the inquiry api response
*/
    public static void CreateInquiryRecord(InquiryResponseWrapper inquiryResponseObj, String inquiryId, Id recordId, String objectName)
    {
        try
        {   
            List<String> listInquiryFieldNames = new List<String>{
                'CreditDevice__Inquiry_ID__c','CreditDevice__Company_Name__c','CreditDevice__Address__c','CreditDevice__City__c',
                'CreditDevice__Country__c','CreditDevice__Entity_created_at__c','CreditDevice__Inquiry_finished_at__c',
                'CreditDevice__Product_code__c','CreditDevice__Amount__c','CreditDevice__Status_code__c','CreditDevice__Priority__c',
                'CreditDevice__Language_code__c','CreditDevice__Reference__c','CreditDevice__Reason__c'};

            if('Account'.EqualsIgnoreCase(objectName)){
                listInquiryFieldNames.add('CreditDevice__Account__c');
            }
            else if('Lead'.EqualsIgnoreCase(objectName)){
                listInquiryFieldNames.add('CreditDevice__Lead__c');
            }
            Util.checkPermissions(new Inquiry__c(),'Insert',listInquiryFieldNames);     
            InquiryWrapper inquryObj = inquiryResponseObj.item;
            Inquiry__c inquiryObj = new Inquiry__c();

            if('Account'.EqualsIgnoreCase(objectName)){
                inquiryObj.Account__c = recordId;
            }
            else if('Lead'.EqualsIgnoreCase(objectName)){
                inquiryObj.Lead__c = recordId;
            }
            //inquiryObj.Account__c = ('Account'.EqualsIgnoreCase(objectName) ? recordId : null);
            //inquiryObj.Lead__c = ('Lead'.EqualsIgnoreCase(objectName) ? recordId : null);          
            inquiryObj.Inquiry_ID__c = inquiryId;
            inquiryObj.Company_Name__c = inquryObj.name;            
            inquiryObj.Address__c = inquryObj.address;            
            inquiryObj.City__c = inquryObj.city;
            inquiryObj.Country__c = inquryObj.country;
            inquiryObj.Entity_created_at__c = inquryObj.created_at;
            inquiryObj.Inquiry_finished_at__c = inquryObj.finished_at;
            inquiryObj.Product_code__c = inquryObj.product;
            inquiryObj.Amount__c = inquryObj.amount;
            inquiryObj.Status_code__c = inquryObj.status;
            inquiryObj.Priority__c = inquryObj.priority;
            inquiryObj.Language_code__c = inquryObj.language;
            inquiryObj.Reference__c = inquryObj.reference;
            inquiryObj.Reason__c = inquryObj.reason; 

            insert inquiryObj;            
        }
        catch(DMLException dx)
        {
            throw dx;
        }       
    }
    /*
* Description: This method is used to fetch inquiry once the inquiry has been placed
*/
    public static HttpResponse FetchInquiry(String inquiryId){
        HttpResponse inquiryResponse;
        try
        {
            Http httpInquiry = new Http();
            HttpRequest request = new HttpRequest(); 
            String apiName ='inquiries/'+inquiryId;
            request = Util.GetGeneratedRequest(null,apiName,null ,'GET');           
            inquiryResponse = httpInquiry.send(request);             
        }
        catch(Exception e)
        {
            throw e;
        }       
        return inquiryResponse;
    }
    /*
* Description: This method is used to generate the request to get the report based on the inquiry placed
*/    
    public static HttpRequest CreateCreditReportRequest(String currentInquiryId){
        HttpRequest request = new HttpRequest();
        Map<String,string> mapRequestHeader = new Map<String,string>();
        mapRequestHeader.put('Content-Type','application/json');
        string apiName = 'inquiries/'+currentInquiryId+'/report';           
        request = Util.GetGeneratedRequest(mapRequestHeader,apiName,null ,'GET');
        request.setTimeout(120000);              
        return request;
    }
    
    public class InquiryWrapper{
        @auraEnabled String id{get;set;}
        @auraEnabled String product{get;set;}
        @auraEnabled String country{get;set;}
        @auraEnabled String country_code{get;set;}
        @auraEnabled String reg_number{get;set;}
        @auraEnabled String language{get;set;}
        @auraEnabled String priority{get;set;}
        @auraEnabled String company_id{get;set;}
        @auraEnabled String vat_number{get;set;} 
        @auraEnabled String name{get;set;}
        @auraEnabled String city{get;set;}
        @auraEnabled String zipcode{get;set;}
        @auraEnabled String address{get;set;}
        @auraEnabled String tel_number{get;set;}
        @auraEnabled Decimal amount{get;set;}
        @auraEnabled String reference{get;set;}
        @auraEnabled String status{get;set;}
        @auraEnabled String created_at{get;set;}
        @auraEnabled String reason{get;set;}
        @auraEnabled String finished_at{get;set;}        
        
        public InquiryWrapper(String product, String country, String reg_number, String language, String priority, String company_id, String vat_number)
        {
            this.product = product;
            this.country = country;
            this.reg_number = reg_number;
            this.language = language;
            this.priority = priority;
            this.company_id = company_id;   
            this.vat_number = vat_number;
        }
    }
    public class InquiryResponseWrapper
    { 
        @auraEnabled InquiryWrapper item{get;set;}
    }
    public class CreditReportResponseWrapper
    {       
        @auraEnabled ReviewWrapper review{get;set;}   
        @auraEnabled PaymentWrapper payments{get;set;}
        String reportLink{get;set;}
    }
    public class ReviewWrapper
    {
        string rating{get;set;}  
        Decimal score{get;set;}  
        string limit_advice{get;set;} 
        string risk{get;set;}
    }
    public class PaymentWrapper
    {
        Decimal score{get;set;}
    }
}