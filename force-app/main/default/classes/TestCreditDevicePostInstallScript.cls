@isTest
public class TestCreditDevicePostInstallScript {
    static testMethod void setup(){
        Integer beforeInstallDMLLimit = Limits.getDMLStatements();
        CreditDevicePostInstallScript postInstallScript = new CreditDevicePostInstallScript();
        Test.testInstall(postInstallScript, null); // This will be execute at  first time installation
        system.assert(Limits.getDMLStatements() > beforeInstallDMLLimit);
    }
}