/***************************************************************
Class : MonitoringInstantUpdateScheduler 
Developer : Sneha U.
Created Date : 03/25/19
Summary : This class is used to schedule monitoring for accounts & leads
Change Log: 
v1.0 - 03/25/19 - Sneha U. - Initial Creation.(100% Code Coverage)
*****************************************************************/
global class MonitoringInstantUpdateScheduler implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
            Database.executeBatch(new MonitoringInstantUpdateBatch('Account'),10);
    }
}