@isTest
public class TestReportViewController {
    @testSetup static void setup() {
        Credit_Device_Custom_Setting__c creds = new Credit_Device_Custom_Setting__c();
        creds.Name = 'CreditDeviceCreds';
        creds.Username__c = 'Test';
        creds.Password__c ='Test@1234';
        creds.Endpoint_URL__c = 'https://inquiry.creditandcollection.nl/api';
        insert creds;
    }
    static testmethod void TestgetReportData(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        ReportViewController reportView = new ReportViewController();
        reportView.InquiryId ='1';
        System.assertEquals('1',reportView.InquiryId);
        reportView.getReportData();
        Test.stopTest();
    }
    static testmethod void TestgetReportDatawithNoId(){
        Test.startTest();
        ReportViewController reportView = new ReportViewController();
        System.assertNotEquals(null,reportView);
        reportView.getReportData();
        Test.stopTest();
    }
    static testmethod void TestgetReportDatawithNullCheck(){
        Test.startTest();
        ReportViewController reportView = new ReportViewController();
        System.assertNotEquals(null,reportView);
        reportView.InquiryId ='null';
        reportView.getReportData();
        Test.stopTest();
    }
}