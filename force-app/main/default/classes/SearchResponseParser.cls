/***************************************************************
Class : SearchResponseParser
Developer : KalpaanG
Summary : This class is used for pasring the Search API result
Change Log: 
v1.0 - 02/14/2019 - KalpanaG - Created this new controller.(93% Code Coverage)
*****************************************************************/
public class SearchResponseParser 
{
    public List<listWrapper> list1 {get;set;}
    public metaWrapper meta {get;set;}
    public class listWrapper 
    {
        public String id {get;set;}
        public String name {get;set;}
        public String reg_number {get;set;}
        public String est_number {get;set;}
        public String vat_number {get;set;}
        public String address {get;set;}
        public String zipcode {get;set;}
        public String city {get;set;}
        public String country {get;set;}
        public String additional {get;set;}
        public String comment {get;set;}
        public Boolean active {get ; set;}
        }
    public class metaWrapper 
    {
        public Integer items;
        public Integer current_page;
        public boolean more_items {get;set;}
    }
    public static SearchResponseParser parse(String json)
    {
        return (SearchResponseParser) System.JSON.deserialize(json, SearchResponseParser.class);
    }
}