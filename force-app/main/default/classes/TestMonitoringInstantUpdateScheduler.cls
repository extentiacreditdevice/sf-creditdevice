@isTest
public class TestMonitoringInstantUpdateScheduler{
    
    @testSetup static void setup() {
        Credit_Device_Custom_Setting__c creds = new Credit_Device_Custom_Setting__c();
        creds.Name = 'CreditDeviceCreds';
        creds.Username__c = 'Test';
        creds.Password__c ='Test@1234';
        creds.Endpoint_URL__c = 'https://inquiry.creditandcollection.nl/api/';
        insert creds;
        Credit_Device_General_Setting__c generalSetting = new Credit_Device_General_Setting__c();
        generalSetting.Name = 'GeneralSetting';
        generalSetting.Disable_Lead__c = false;
        generalSetting.Dutch_Country_Code__c = 'NL';
        generalSetting.Dutch_Language_Code__c ='NL';
        generalSetting.International_Product_Code__c = '701';
        generalSetting.Account_Country_Field__c='BillingCountry';
        generalSetting.Lead_Country_Field__c = 'Country';
        generalSetting.National_Product_Code__c ='702';
        generalSetting.Non_Dutch_Language_Code__c ='EN';
        generalSetting.CreditDevice__Alert_Type_ALL__c ='INSOLVENCY,REVIEW_INCREASE,REVIEW_DECREASE,TERMINATION';
        generalSetting.CreditDevice__Alert_Type_HQ__c ='NAW';
        generalSetting.CreditDevice__Monitoring_Batch_Size__c=10;
        insert generalSetting;
        
        List<FieldMapping__c> fieldMappingList = new List<FieldMapping__c>();
        FieldMapping__c fieldmapping1 = new fieldMapping__c();
        fieldmapping1.Name ='Account-name';
        fieldmapping1.SF_Fields__c ='name';
        fieldmapping1.API_Fields__c = 'contact_info/name';
        fieldmapping1.Object__c = 'Account';
        fieldMappingList.add(fieldmapping1);
        FieldMapping__c fieldmapping11 = new fieldMapping__c();
        fieldmapping11.Name ='Account-numberofemployees';
        fieldmapping11.SF_Fields__c ='numberofemployees';
        fieldmapping11.API_Fields__c = 'contact_info/fax_number';
        fieldmapping11.Object__c = 'Account';
        fieldMappingList.add(fieldmapping11);
        
        FieldMapping__c fieldmapping2 = new fieldMapping__c();
        fieldmapping2.Name ='Account-Description';
        fieldmapping2.SF_Fields__c ='description';
        fieldmapping2.API_Fields__c = 'contact_info/tradenames[]';
        fieldmapping2.Object__c = 'Account';
        fieldMappingList.add(fieldmapping2);
        
        FieldMapping__c fieldmapping3 = new fieldMapping__c();
        fieldmapping3.Name ='Lead-lastname';
        fieldmapping3.SF_Fields__c ='lastname';
        fieldmapping3.API_Fields__c = 'contact_info/name';
        fieldmapping3.Object__c = 'Lead';
        fieldMappingList.add(fieldmapping3);
        
        FieldMapping__c fieldmapping4 = new fieldMapping__c();
        fieldmapping4.Name ='AccountND-name';
        fieldmapping4.CreditDevice__International__c=true;
        fieldmapping4.SF_Fields__c ='name';
        fieldmapping4.API_Fields__c = 'name';
        fieldmapping4.Object__c = 'Account';
        fieldMappingList.add(fieldmapping4);
        
        FieldMapping__c fieldmapping5 = new fieldMapping__c();
        fieldmapping5.Name ='LeadND-lastname';
        fieldmapping5.CreditDevice__International__c=true;
        fieldmapping5.SF_Fields__c ='lastname';
        fieldmapping5.API_Fields__c = 'name';
        fieldmapping5.Object__c = 'Lead';
        fieldMappingList.add(fieldmapping5);

        insert fieldMappingList;
        List<CreditDevice__Country__c> countryList = new List<CreditDevice__Country__c>();
        CreditDevice__Country__c country1 = new CreditDevice__Country__c();
        country1.CreditDevice__Country_Code__c ='NL';
        country1.Name = 'Netherlands';
        countryList.add(country1);
        
        CreditDevice__Country__c country2 = new CreditDevice__Country__c();
        country2.CreditDevice__Country_Code__c ='IN';
        country2.Name = 'India';
        countryList.add(country2);
        insert countryList;
        
        List<Lead> listObjectsToInsert = new List<Lead>();

        for(Integer i=1;i<=10;i++)
        {
            Lead ld = new Lead();
            ld.LastName = 'Test Lead'+i;
            ld.Company = 'Test Company'+1;
            ld.Country ='NL';
            ld.CreditDevice__Registration_Number__c = '12345'+i;
            //ld.CreditDevice__Alerts_Exist__c = true;
            ld.CreditDevice__Company_Id__c ='354768GABCD456378';
            ld.CreditDevice__Credit_Limit_Change__c=false;
            listObjectsToInsert.add(ld);
        }
        insert listObjectsToInsert;  
        
        List<Sobject> listAlertsToInsert = new List<Sobject>();

        for(Lead objLead:listObjectsToInsert)
        {
            CreditDevice__Alert__c alert = new CreditDevice__Alert__c(CreditDevice__Lead_Id__c=objLead.id,CreditDevice__Credit_Limit_Change__c=true);
            listAlertsToInsert.add(alert);
        }
        insert listAlertsToInsert;    
        
        List<Account> listAccountsToInsert = new List<Account>();

        for(Integer i=1;i<=10;i++)
        {
            Account objAccount = new Account();
            objAccount.Name = 'Test Account'+i;
            objAccount.BillingCountry ='NL';
            objAccount.CreditDevice__Registration_Number__c = '12345'+i;
            objAccount.CreditDevice__Company_Id__c ='354768GABCD456378';
            objAccount.CreditDevice__Credit_Limit_Change__c=false;
            listAccountsToInsert.add(objAccount);
        }
        listAccountsToInsert[0].CreditDevice__Est_Number__c = '000021730873';
        insert listAccountsToInsert;  
        
        listAlertsToInsert = new List<CreditDevice__Alert__c>();

        for(Account objAccount:listAccountsToInsert)
        {
            CreditDevice__Alert__c alert = new CreditDevice__Alert__c(CreditDevice__Account_Id__c=objAccount.id,CreditDevice__Credit_Limit_Change__c=true);
            listAlertsToInsert.add(alert);
        }
        insert listAlertsToInsert;    
    }

    static testmethod void testScheduleLead() 
    {
        String objName= 'Lead';
        String CRON_EXP = '0 0 0 15 3 ? *';
        Test.startTest();
        String DutchCountryCode = MonitoringUtility.DutchCountryCode;
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        String jobId = System.schedule('Monitoring Batch Class Lead',  CRON_EXP, new MonitoringInstantUpdateScheduler()); 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        
        Database.executeBatch(new MonitoringBatch('Lead'),10);
        Test.stopTest();
    }
    static testmethod void testInstantUpdateLead() 
    {
        Test.startTest();
        String DutchCountryCode = MonitoringUtility.DutchCountryCode;
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        Database.executeBatch(new MonitoringInstantUpdateBatch('Lead'),10);
        Test.stopTest();
    }
    static testmethod void testScheduleAccount() 
    {
        String objName= 'Account';
        String CRON_EXP = '0 0 0 15 3 ? *';
        Test.startTest();
        String DutchCountryCode = MonitoringUtility.DutchCountryCode;
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        String jobId = System.schedule('Monitoring Batch Class Account',  CRON_EXP, new MonitoringInstantUpdateScheduler()); 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        
        Database.executeBatch(new MonitoringBatch(objName),10);
        Test.stopTest();
    }
    static testmethod void testInstantUpdateAccount() 
    {
        Test.startTest();
        String DutchCountryCode = MonitoringUtility.DutchCountryCode;
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        Database.executeBatch(new MonitoringInstantUpdateBatch('Account'),10);
        Test.stopTest();
    }
    static testmethod void testPprocessAlerts()
    {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        MonitoringUtility mUtility = New MonitoringUtility();
        MonitoringUtility.processAlerts(Date.today());
        Test.stopTest();
    }
}