/***************************************************************
Class : MonitoringBatch
Developer : Sneha U.
Created Date : 03/25/19
Summary : This class is used to activate Account/Leads for monitoring and update if any alerts exist
Change Log: 
v1.0 - 03/25/19 - Sneha U. - Initial Creation.(100% Code Coverage)
v1.1 - 06/12/19 - Kalpana G. - Add disable Lead functionality check(100% Code Coverage)
*****************************************************************/
global class MonitoringBatch  implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    global final String query;
    global final String objectName;
    global final Date alertProcessDate;
    global Integer failedRecordCount = 0;    
    global Integer successRecordCount = 0;    
    global Integer totalRecordCount = 0; 
    global Integer monitoringEntryCount = 0;   
    global Integer monitoringEntryFailedCount = 0;
    global MonitoringBatch(String objName)
    {
        objectName = objName;
        alertProcessDate = Date.today();
        //Get all accounts which are not set up for Monitoring
        query='select id,CreditDevice__'+objectName+'_Monitoring__c,CreditDevice__Registration_Number__c from '+objectName+' where CreditDevice__Company_Id__c!=null and CreditDevice__Is_International__c=false and CreditDevice__Registration_Number__c!=null and CreditDevice__'+objectName+'_Monitoring__c=false'; 
    }
    global MonitoringBatch(String objName,Date aDate)
    {
        objectName = objName;
        alertProcessDate = aDate;
        //Get all accounts which are not set up for Monitoring
        query='select id,CreditDevice__'+objectName+'_Monitoring__c,CreditDevice__Registration_Number__c from '+objectName+' where CreditDevice__Company_Id__c!=null and CreditDevice__Is_International__c=false and CreditDevice__Registration_Number__c!=null and CreditDevice__'+objectName+'_Monitoring__c=false'; 
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        try
        {            
            List<SObject> listCompanyToUpdate = new List<SObject>();
            for(SObject objCompany: scope)
            {
                //Add endpoint
                string endpoint='monitor/entries/'+objCompany.get('CreditDevice__Registration_Number__c');
                String monitoringFieldName = 'CreditDevice__'+objectName+'_Monitoring__c';
                objCompany.put(monitoringFieldName,MonitoringUtility.processMonitoringEnrty(endpoint));  
                //Update account if Monitoring entry is successful
                
                if(Boolean.valueOf(objCompany.get(monitoringFieldName)) )
                {
                    listCompanyToUpdate.add(objCompany);
                    monitoringEntryCount+=1;
                }
                else
                {
                    monitoringEntryFailedCount+=1;
                }

            }            
            if(listCompanyToUpdate.size()>0)
            {               
                List<Database.SaveResult> listUpdatedRecords = database.update(listCompanyToUpdate,false);
                for (Database.SaveResult sr : listUpdatedRecords) 
                {
                    if (!sr.isSuccess()) 
                    {
                        failedRecordCount+=1;
                    }
                    else
                    {
                        successRecordCount+=1;
                    }                    
                }
            }
            totalRecordCount += scope.size();
        }
        catch(Exception e)
        {
            AsyncApexJob asyn = [SELECT Id, Status, CreatedBy.Email, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE Id=:BC.getJobId() limit 1];
            String subject = objectName + ' Monitoring Entry Batch Report';
            Util.SendEmailToAdminUser(asyn,e.getMessage(),subject);
        }
        
    }
    global void finish(Database.BatchableContext BC)
    {
        Credit_Device_General_Setting__c generalSetting  = Credit_Device_General_Setting__c.getValues('GeneralSetting');
        Boolean isLeadDisabled = false;
        Integer batchsize = 10;
        
        if(generalSetting!=null)
        {
            isLeadDisabled = generalSetting.CreditDevice__Disable_Lead__c;
            
            //Get the batchSize
            if(generalSetting.CreditDevice__Monitoring_Batch_Size__c!=null && generalSetting.CreditDevice__Monitoring_Batch_Size__c!=0){
                batchsize=Integer.valueOf(generalSetting.CreditDevice__Monitoring_Batch_Size__c);
            }
        }
        //Execute for Leads once Acount batch is completed if lead functionality is not disabled.
        if(objectName.equalsIgnoreCase('Account') && !Test.isRunningTest() && !isLeadDisabled)
        {
            Database.executeBatch(new MonitoringBatch('Lead',alertProcessDate),batchSize);
        }
        if(objectName.equalsIgnoreCase('Account') && !Test.isRunningTest() && isLeadDisabled)
        {
            MonitoringUtility.processAlerts(alertProcessDate); 
        }
        //Process Alerts once Leads are set for Monitoring
        if(objectName.equalsIgnoreCase('Lead') && !Test.isRunningTest() && !isLeadDisabled)
        {
            MonitoringUtility.processAlerts(alertProcessDate);  
        }

        Boolean isEmailNotificationDisabled;
        if(generalSetting!=null)
        {
            isEmailNotificationDisabled = generalSetting.Email_Notification_Disabled__c;
        }
        if(!isEmailNotificationDisabled)
        {            
            AsyncApexJob asyn = [SELECT Id, Status, CreatedBy.Email,CreatedBy.Name, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE Id=:BC.getJobId() limit 1];
            String subject = objectName + ' Monitoring Entry Batch Report';
            String emailBody = 'Hi '+asyn.CreatedBy.Name + '\n\n';
            emailBody += 'The batch apex job '+ asyn.Id+' is processed. \n\n';
            emailBody += 'Batch Status: ' + asyn.Status+'\n';
            emailBody += 'No. of records processed for monitoring entry: '+ monitoringEntryCount+'\n';
            emailBody += 'No.of records failed for monitoring entry: '+monitoringEntryFailedCount+'\n';
            emailBody += 'No. of records processed: '+successRecordCount + '\n';
            emailBody += 'No. of records failed: '+failedRecordCount;
            Util.SendEmailToAdminUser(asyn,emailBody,subject);
        }
    }
}