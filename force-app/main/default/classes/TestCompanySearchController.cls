@isTest
public class TestCompanySearchController {

    @testSetup static void setup() {
        Credit_Device_Custom_Setting__c creds = new Credit_Device_Custom_Setting__c();
        creds.Name = 'CreditDeviceCreds';
        creds.Username__c = 'Test';
        creds.Password__c ='Test@1234';
        creds.Endpoint_URL__c = 'https://inquiry.creditandcollection.nl/api';
        insert creds;
        Lead ld = new Lead();
        ld.LastName = 'Test Lead';
        ld.Company = 'Test Company';
        ld.Country ='NL';
        ld.CreditDevice__Registration_Number__c = '12345678';
        ld.CreditDevice__Company_Id__c ='354768GABCD456378';
        ld.CreditDevice__Credit_Limit_Change__c=false;
        insert ld;
        
        Account acc = new Account();
        acc.Name= 'Test account';
        acc.BillingCity   = 'Amsterdam';
        acc.CreditDevice__Company_Id__c ='l5eG0lBqUSUTshaAs4cioA';
        acc.CreditDevice__Is_International__c=true;
        acc.BillingPostalCode  ='1000AA';
        acc.BillingCountry='US';
        acc.CreditDevice__Registration_Number__c= '1234567HG';
        insert acc;
        
        Credit_Device_General_Setting__c generalSetting = new Credit_Device_General_Setting__c();
        generalSetting.Name = 'GeneralSetting';
        generalSetting.Disable_Lead__c = false;
        generalSetting.Dutch_Country_Code__c = 'NL';
        generalSetting.Dutch_Language_Code__c ='NL';
        generalSetting.International_Product_Code__c = '701';
        generalSetting.National_Product_Code__c ='702';
        generalSetting.Non_Dutch_Language_Code__c ='EN';
        generalSetting.CreditDevice__Alert_Type_ALL__c ='INSOLVENCY,REVIEW_INCREASE,REVIEW_DECREASE,TERMINATION';
        generalSetting.CreditDevice__Alert_Type_HQ__c ='NAW';
        insert generalSetting;
        List<FieldMapping__c> fieldMappingList = new List<FieldMapping__c>();
        FieldMapping__c fieldmapping1 = new fieldMapping__c();
        fieldmapping1.Name ='Account-name';
        fieldmapping1.SF_Fields__c ='name';
        fieldmapping1.API_Fields__c = 'contact_info/name';
        fieldmapping1.Object__c = 'Account';
        fieldMappingList.add(fieldmapping1);
        FieldMapping__c fieldmapping11 = new fieldMapping__c();
        fieldmapping11.Name ='Account-numberofemployees';
        fieldmapping11.SF_Fields__c ='numberofemployees';
        fieldmapping11.API_Fields__c = 'contact_info/fax_number';
        fieldmapping11.Object__c = 'Account';
        fieldMappingList.add(fieldmapping11);
        
        FieldMapping__c fieldmapping2 = new fieldMapping__c();
        fieldmapping2.Name ='Account-Description';
        fieldmapping2.SF_Fields__c ='description';
        fieldmapping2.API_Fields__c = 'contact_info/tradenames[]';
        fieldmapping2.Object__c = 'Account';
        fieldMappingList.add(fieldmapping2);
        
        FieldMapping__c fieldmapping3 = new fieldMapping__c();
        fieldmapping3.Name ='Lead-lastname';
        fieldmapping3.SF_Fields__c ='lastname';
        fieldmapping3.API_Fields__c = 'contact_info/name';
        fieldmapping3.Object__c = 'Lead';
        fieldMappingList.add(fieldmapping3);
        
        FieldMapping__c fieldmapping4 = new fieldMapping__c();
        fieldmapping4.Name ='AccountND-name';
        fieldmapping4.CreditDevice__International__c=true;
        fieldmapping4.SF_Fields__c ='name';
        fieldmapping4.API_Fields__c = 'name';
        fieldmapping4.Object__c = 'Account';
        fieldMappingList.add(fieldmapping4);
        
        FieldMapping__c fieldmapping5 = new fieldMapping__c();
        fieldmapping5.Name ='LeadND-lastname';
        fieldmapping4.CreditDevice__International__c=true;
        fieldmapping5.SF_Fields__c ='lastname';
        fieldmapping5.API_Fields__c = 'name';
        fieldmapping5.Object__c = 'Lead';
        fieldMappingList.add(fieldmapping5);
        
        FieldMapping__c fieldmapping6 = new fieldMapping__c();
        fieldmapping6.Name ='Contact-birthdate';
        fieldmapping6.SF_Fields__c ='birthdate';
        fieldmapping6.API_Fields__c = 'registration/dates[]/settlement';
        fieldmapping6.Object__c = 'Contact';
        fieldMappingList.add(fieldmapping6);
        
        FieldMapping__c fieldmapping7 = new fieldMapping__c();
        fieldmapping7.Name ='ContactND-firstname';
        fieldmapping4.CreditDevice__International__c=true;
        fieldmapping7.SF_Fields__c ='firstname';
        fieldmapping7.API_Fields__c = 'name';
        fieldmapping7.Object__c = 'Contact';
        fieldMappingList.add(fieldmapping7);
        
        insert fieldMappingList;
        List<CreditDevice__Country__c> countryList = new List<CreditDevice__Country__c>();
        CreditDevice__Country__c country1 = new CreditDevice__Country__c();
        country1.CreditDevice__Country_Code__c ='NL';
        country1.Name = 'Netherlands';
        countryList.add(country1);
        
        CreditDevice__Country__c country2 = new CreditDevice__Country__c();
        country2.CreditDevice__Country_Code__c ='IN';
        country2.Name = 'India';
        countryList.add(country2);
        insert countryList;
    }
    
    public static testmethod void testSearchAndFetchReportforDutch()
    {
       
        String json=        '{'+
        '    "list1": ['+
        '        {'+
        '            "id": "l5eG0HABCDGaAs4cioA==",'+
        '            "name": "Test",'+
        '            "reg_number": "87000046",'+
        '            "est_number": "000028000062",'+
        '            "vat_number": null,'+
        '            "address": "Test",'+
        '            "zipcode": "1079LS",'+
        '            "city": "Amsterdam",'+
        '            "country": "NL",'+
        '            "additional": "Hoofdvestiging"'+
        '        }'+
        '    ],'+
        '    "meta": {'+
        '        "items": 10,'+
        '        "current_page": 1,'+
        '        "more_items": true'+
        '    }'+
        '}';
        
        CompanySearchController compctrl = new CompanySearchController();
        compctrl.getSelectedCountry();
        System.assertEquals(true,compctrl.isVatDisabled);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        compctrl.searchTerm ='Amit';
        compctrl.searchResult();
        System.assertEquals(1, compctrl.ListSearchCompanyWrapper.size());
        System.assertNotEquals(0, compctrl.MapOfSfFieldsToApiField.get('Account').size());
        SearchResponseParser parsedRespose1 = SearchResponseParser.parse(json);       
        compctrl.isImportWithRT = true;
        CompanySearchController.searchCompanyWrapper searchWrapper1 = new CompanySearchController.searchCompanyWrapper(true,false,false,false,null,'',parsedRespose1.list1[0],new List<Contact>(),new Map<String, Object>(),0);
        CompanySearchController.searchCompanyWrapper searchWrapper2 = new CompanySearchController.searchCompanyWrapper(false,true,false,false,null,'',parsedRespose1.list1[0],new List<Contact>(),new Map<String, Object>(),1);
        CompanySearchController.searchCompanyWrapper searchWrapper3 = new CompanySearchController.searchCompanyWrapper(false,false,true,false,null,'',parsedRespose1.list1[0],new List<Contact>(),new Map<String, Object>(),2);
        compctrl.SetUpdatedContact();
        compctrl.ListSearchCompanyWrapper.add(searchWrapper1);
        compctrl.ListSearchCompanyWrapper.add(searchWrapper2);
        compctrl.ListSearchCompanyWrapper.add(searchWrapper3);
        compctrl.fetchInstantReport();
        System.assertNotEquals(0, compctrl.ListAccount.size());
        System.assertNotEquals(0, compctrl.ListLead.size());
        Test.StopTest(); 
        compctrl.clearResult();
    }
    public static testmethod void testSearchAndFetchReportforNonDutch()
    {
        String json=        '{'+
        '    "list1": ['+
        '        {'+
        '            "id": "l5eG0HDGABCDaAs4cioA==",'+
        '            "name": "Test",'+
        '            "reg_number": "87000046",'+
        '            "est_number": "000056400005",'+
        '            "vat_number": null,'+
        '            "address": "Test",'+
        '            "zipcode": "411011",'+
        '            "city": "Pune",'+
        '            "country": "IN",'+
        '            "additional": "Hoofdvestiging"'+
        '        }'+
        '    ],'+
        '    "meta": {'+
        '        "items": 10,'+
        '        "current_page": 1,'+
        '        "more_items": true'+
        '    }'+
        '}';
        
        CompanySearchController compctrl = new CompanySearchController();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        compctrl.searchTerm ='Amit';
        compctrl.searchResult();
        System.assertEquals(1, compctrl.ListSearchCompanyWrapper.size());
        compctrl.getFieldMapping('IN');
        System.assertNotEquals(0, compctrl.MapOfSfFieldsToApiField.get('Account').size());
        SearchResponseParser parsedRespose1 = SearchResponseParser.parse(json);
        compctrl.isImportWithRT = true;
        compctrl.ListSearchCompanyWrapper.clear();
        CompanySearchController.searchCompanyWrapper searchWrapper1 = new CompanySearchController.searchCompanyWrapper(true,false,false,false,null,'',parsedRespose1.list1[0],new List<Contact>(),new Map<String, Object>(),0);
        CompanySearchController.searchCompanyWrapper searchWrapper2 = new CompanySearchController.searchCompanyWrapper(false,true,false,false,null,'',parsedRespose1.list1[0],new List<Contact>(),new Map<String, Object>(),1);
        CompanySearchController.searchCompanyWrapper searchWrapper3 = new CompanySearchController.searchCompanyWrapper(true,false,true,false,null,'',parsedRespose1.list1[0],new List<Contact>(),new Map<String, Object>(),2);
        
        compctrl.ListSearchCompanyWrapper.add(searchWrapper1);
        compctrl.ListSearchCompanyWrapper.add(searchWrapper2);
        compctrl.ListSearchCompanyWrapper.add(searchWrapper3);
        compctrl.isQuickImport = true;
        compctrl.fetchInstantReport();
        System.assertNotEquals(0, compctrl.ListAccount.size());
        Test.StopTest(); 
        compctrl.clearResult();
        System.assertEquals(0, compctrl.ListSearchCompanyWrapper.size());
    }
    public static testmethod void testCommonMethods()
    {
        CompanySearchController compctrl = new CompanySearchController();
        SearchResponseParser responseparser = new SearchResponseParser();
        SearchResponseParser.metaWrapper metaWrapper1 = new SearchResponseParser.metaWrapper();
        metaWrapper1.items =10;
        metaWrapper1.current_page = 1;
        metaWrapper1.more_items = false;
        SearchResponseParser.listWrapper  listWrapper1 = new SearchResponseParser.listWrapper();
        listWrapper1.id ='1234';
        listWrapper1.name='Test';
        listWrapper1.reg_number='87654321';
        listWrapper1.est_number =null;
        listWrapper1.vat_number =  null;
        listWrapper1.address = null;
        listWrapper1.zipcode = null;
        listWrapper1.city = null;
        listWrapper1.country = 'NL';
        listWrapper1.additional =null;
        list<SelectOption> accRecordTypeList = compctrl.getAccountRecordTypes();
        list<SelectOption> leadRecordTypeList = compctrl.getLeadRecordTypes();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        compctrl.getNext();
        System.assertEquals(2, compctrl.pageNumber);
        compctrl.getPrevious();
        System.assertEquals(1, compctrl.pageNumber);
        compctrl.goToBackPage();
        System.assertEquals(false, compctrl.isImportSelected);
        System.assertEquals(false, compctrl.isImportWithRT);
        System.assertEquals(false, compctrl.isQuickImport);
        Test.StopTest(); 
    }
    public static testmethod void testSaveRecords()
    {
        Test.startTest();
        CompanySearchController compctrl = new CompanySearchController();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        compctrl.activeTab ='AdvancedSearch';
        compctrl.name='Amit';
        compctrl.searchResult();
        System.assertEquals('NL', compctrl.parsedResponse.list1[0].country);
        Account acc = new Account();
        acc.Name='Test Account';
        compctrl.MapOfAccounts.put('59623977000028898362',(Account) acc);
        Lead ld = new Lead();
        ld.LastName='Test Lead';
        compctrl.MapOfLeads.put('TestLead',(Lead) ld);
        Contact con = new Contact();
        con.FirstName ='Test Contact';
        CompanySearchController.searchCompanyWrapper searchWrapper = new CompanySearchController.searchCompanyWrapper(false,false,true,false,null,'',compctrl.parsedResponse.list1[0],new List<Contact>{con},new Map<String, Object>(),0);
        compctrl.ListSearchCompanyWrapper.add(searchWrapper);
        compctrl.saveResult();
        System.assertEquals(1, compctrl.MapOfAccounts.size());
        System.assertEquals(1, compctrl.MapOfLeads.size());
        Test.stopTest();
    }
    public static testmethod void testcheckDuplicates()
    {
        Test.startTest();
        List<String> companyIdList = new List<String>{'l5eG0lBqUSUTshaAs4cioA','354768GABCD456378'};
        Map<String,Id> mapOfCompanyIdtoRecId = Util.checkDuplicates(companyIdList);
        Test.stopTest();
    }  
    public static testmethod void checkPermissions()
    {
        Test.startTest();
        List <String> listFieldApiNames = new List<String>();
        Util.checkPermissions(new Account(), 'Upsert', listFieldApiNames); 
        Test.stopTest();
    } 
}