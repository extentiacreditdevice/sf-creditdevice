/***************************************************************
Class : AppConfigurationController
Developer : RahulG
Created Date : 02/14/2019
Summary : This class is used for mapping the SF fields with the API fields.(created with "without sharing" as it is only used to insert/update custom setting data).
Change Log: 
v1.0 - 02/14/2019 - RahulG - Created this new controller.(88% Code Coverage)
v1.1 - 02/26/2019 - Karan V - added Lightning UI checker code.
v1.2 - 02/27/2019 - Karan V - added 'Add row' button disabled checker code. Removed 'Add row' button disabled checker code.
v1.3 - 03/05/2019 - Karan V - Removed comments, system debug code, etc .
v1.4 - 03/05/2019 - Karan V - Updated Controller.
v1.5 - 03/14/2019 - Sneha U. - Add Code for Credential Tab
v1.6 - 03/19/2019 - Kalpna G - Save mapping bug fixes
v1.7 - 03/25/2019 - Kalpna G - Reflect all API fields as fixed output text
v1.8 - 05/06/2019 - Sneha U. - Add Non Dutch checkbox field check instead of NDutch string
*****************************************************************/

public without sharing class AppConfigurationController 
{
    public String ActiveTab {get;set;}
    public String SelectedObject {get; set;}
    public String SelectedObjectNDutch {get; set;}
    public boolean HasPermission {get;set;}
    public String APIName {get;set;}
    public List<FieldsMappingWrapper> ListFieldsMappingWrap{get;set;}
    public Map<String,String> MapOfApiPathtoName{get;set;}
    public Map<String,String> MapOfApiPathtoNameNDutch{get;set;}
    public Map<String,Integer> MapOfApiPathToOrderNumber = new Map<String,Integer>();
    public Map<String,Integer> MapOfApiPathToOrderNumberNDutch = new Map<String,Integer>();
    public List<FieldsMappingWrapperNDutch> ListFieldsMappingWrapNDutch{get;set;}
    public List<FieldMapping__c> ListFieldMappingToDelete = new List<FieldMapping__c>();
    public Map <String,Schema.SObjectType> MapOfGlobalDescribeData = new Map <String,Schema.SObjectType>();
    public Credit_Device_Custom_Setting__c ObjSetting{get;set;}
    public Credit_Device_General_Setting__c GeneralSetting {get;set;} //Get details from general setting
    
    //Initialize required attributes
    public AppConfigurationController()
    {
        APIName='';
        ActiveTab ='credentialSetup';
        ListFieldsMappingWrap = new List<FieldsMappingWrapper>();
        MapOfApiPathtoName = new Map<String,String>();
        MapOfApiPathtoNameNDutch = new Map<String,String>();
        ListFieldsMappingWrapNDutch = new List<FieldsMappingWrapperNDutch>();
        SelectedObject = 'Account';
        SelectedObjectNDutch = 'Account';
        List<string> listPermissionSetNames  = new List<String> {'CreditDevice_Admin'};
            HasPermission = Util.HasPermission(listPermissionSetNames); // CHeck if user has Admin permission to view the page
        if(!HasPermission && !Test.isRunningTest())
        {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, System.Label.MSG_NO_ACCESS_TO_SECTION));
            return;
        }
        MapOfGlobalDescribeData = MonitoringUtility.MapOfGlobalDescribeData;
        GetFieldsMappingWrapperList(); //get existing mapping for dutch companies
        GetFieldsMappingWrapperListND(); //get existing mapping for international companies 
        ObjSetting = new Credit_Device_Custom_Setting__c(Name='CreditDeviceCreds');
        Credit_Device_Custom_Setting__c credtiDeviceCredentials = new Credit_Device_Custom_Setting__c();
        credtiDeviceCredentials = Credit_Device_Custom_Setting__c.getValues('CreditDeviceCreds'); //Get the existing credential
        if(credtiDeviceCredentials!=null)
        {
            ObjSetting = credtiDeviceCredentials;
        }
        GeneralSetting = Credit_Device_General_Setting__c.getValues('GeneralSetting');
        if(GeneralSetting ==null)
        {
            GeneralSetting = new Credit_Device_General_Setting__c(Name='GeneralSetting');
        }
    }
    /*
* Description: This method is used to save the creditdevice credentials for the API callout
*/    
    public PageReference SaveCredentials()
    {
        try
        {
            if(ObjSetting.Endpoint_URL__c!=null && ObjSetting.Username__c!=null && ObjSetting.Password__c!=null)
            {
                upsert ObjSetting;
                upsert GeneralSetting;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Credentials saved successfully.')); //Show message if the credentials are saved successfully
            }else
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please Enter required fields.')); // Show error when reuwired fields are missing
            }
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        return null;
    }   
    /*
* Description: This method is used to get the existing mapping for Dutch companies
*/    
    public PageReference GetFieldsMappingWrapperList()
    {
        GetApiObjectFields(SelectedObject); //Get Instant API details from custom setting
        ListFieldMappingToDelete.clear();
        List<String> ListexistingApiNames = new List<String>();
        ListFieldsMappingWrap.clear();
        try
        {
            for(FieldMapping__c fieldMappingToIterate : FieldMapping__c.getall().values()) //Get all field mapping records for selected object and add it to wrapperList
            {
                //if(SelectedObject.equalsIgnoreCase(fieldMappingToIterate.Object__c) && !fieldMappingToIterate.Name.contains('NDutch') && !MapOfApiPathtoName.isEmpty() && MapOfApiPathtoName.containsKey(fieldMappingToIterate.CreditDevice__API_Fields__c.toLowerCase()))
                //v1.8 - Add International check
                if(SelectedObject.equalsIgnoreCase(fieldMappingToIterate.Object__c) && !fieldMappingToIterate.CreditDevice__International__c && !MapOfApiPathtoName.isEmpty() && MapOfApiPathtoName.containsKey(fieldMappingToIterate.CreditDevice__API_Fields__c.toLowerCase()))
                {
                    FieldsMappingWrapper fieldsMapWrap = new FieldsMappingWrapper(fieldMappingToIterate.Id,fieldMappingToIterate.CreditDevice__Do_not_overwrite__c,fieldMappingToIterate.CreditDevice__SF_Fields__c.toLowerCase(),fieldMappingToIterate.CreditDevice__API_Fields__c.toLowerCase(),MapOfApiPathtoName.get(fieldMappingToIterate.CreditDevice__API_Fields__c.toLowerCase()),MapOfApiPathToOrderNumber.get(fieldMappingToIterate.CreditDevice__API_Fields__c.toLowerCase()));
                    ListFieldsMappingWrap.add(fieldsMapWrap);
                    ListexistingApiNames.add(fieldMappingToIterate.CreditDevice__API_Fields__c.toLowercase()); 
                }
            }
            if(!MapOfApiPathtoName.isEmpty())
            {
                for(String apiName  : MapOfApiPathtoName.keySet()) //Add none option of option
                {
                    if(!ListexistingApiNames.contains(apiName))
                    {
                        ListFieldsMappingWrap.add(new FieldsMappingWrapper(null,false,'--None--',apiName,MapOfApiPathtoName.get(apiName),MapOfApiPathToOrderNumber.get(apiName.toLowerCase())));
                    }
                }
            }
            ListFieldsMappingWrap.sort(); // Sort the list based on order number
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        return null;
    }
    /*
* Description: This method is used get the list of objects for dutch mapping
*/   
    public List<SelectOption> GetListOfObjects()
    {
        List<SelectOption> ListObjNames = new List<SelectOption>();
        if(GeneralSetting!=null)
        {
            List<String> listObj = GeneralSetting.CreditDevice__Objects_Available_for_Dutch__c.split(',');
            for(String option : listObj)
            {
                ListObjNames.add(new SelectOption(option,option));
            }
        }        
        return ListObjNames;
    }
    /*
* Description: This method is used to get the API fields and path from custom setting for Dutch companies
*/
    public void GetApiObjectFields(String SelectedObject)
    {
        MapOfApiPathtoName.clear();
        MapOfApiPathToOrderNumber.clear();
        List<CreditDevice__APIFields__c > ListAllApiFields = CreditDevice__APIFields__c.getAll().values(); //Get all API fields from custom setting
        ListAllApiFields.sort();
        if(!ListAllApiFields.isEmpty())
        {
            ListAllApiFields.sort();
            for(CreditDevice__APIFields__c field : ListAllApiFields) //create map with Api field path and API field label based on custom setting.
            {
                if(!'Contact'.EqualsIgnoreCase(SelectedObject) && !field.Is_International__c && !field.Available_for_Contact__c)
                {
                    //Add fields and path in map for Account and Lead
                    MapOfApiPathtoName.put(field.Path__c.toLowerCase(),field.Label__c);
                }else if('Contact'.EqualsIgnoreCase(SelectedObject) && !field.Is_International__c && field.Available_for_Contact__c)
                {
                    MapOfApiPathtoName.put(field.Path__c.toLowerCase(),field.Label__c); //Add fields and path in map for Contact
                }
                MapOfApiPathToOrderNumber.put(field.Path__c.toLowerCase(),Integer.ValueOf(field.CreditDevice__Order_Number__c));
            }
        }   
    }
    /*
* Description: This method is used to get fields of selected object using Schema class
*/  
    public List<SelectOption> GetSelectedObjectFields() 
    {
        Map<String, Schema.SObjectType> MapOfSchema = MapOfGlobalDescribeData;
        Schema.SObjectType ObjectSchema = MapOfSchema.get(SelectedObject);
        Map<String, Schema.SObjectField> MapOfField = ObjectSchema.getDescribe().fields.getMap();
        List<SelectOption> ListFieldNamesSelect = new List<SelectOption>();
        ListFieldNamesSelect.add(new SelectOption('--None--','--None--'));
        try{
            for (String fieldName: MapOfField.keySet()) 
            {
                if(MapOfField.get(fieldName).getDescribe().isUpdateable() && MapOfField.get(fieldName).getDescribe().isCreateable())
                {
                    ListFieldNamesSelect.add(new SelectOption(fieldName,MapOfField.get(fieldName).getDescribe().getLabel()));
                }
            }
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        return SortSelectList(ListFieldNamesSelect);
    } 
    /*
* Description: This method is used to sort the API field Label and Value
*/    
    public List<SelectOption> SortSelectList(List<SelectOption> ListFieldNamesSelect)
    {
        List<String> ListToSort = new List<String>();
        List<SelectOption> ListSortedSelectOption =  new List<SelectOption>();
        try
        {
            for(SelectOption option : ListFieldNamesSelect)
            {
                ListToSort.add(option.getLabel()+'##'+option.getValue());
            }
            ListToSort.sort();
            for(String field : ListToSort)
            {
                ListSortedSelectOption.add(new SelectOption(field.split('##')[1],field.split('##')[0]));
            }
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }        
        return ListSortedSelectOption;
    }
    /*
* Description: This method is used to save the mapping based on users current selection for dutch companies
*/    
    public PageReference SaveMapping()
    {
        ListFieldMappingToDelete.clear();
        List<FieldMapping__c> ListFieldMappingToUpsert = new List<FieldMapping__c>();
        try
        {
            if(ListFieldsMappingWrap!=null && !ListFieldsMappingWrap.isEmpty())
            {
                ListFieldMappingToUpsert = ValidateMapping(ListFieldsMappingWrap);
                
                if(ListFieldMappingToUpsert!=null && ListFieldMappingToUpsert.Size()>0)
                {
                    upsert ListFieldMappingToUpsert;
                    Delete ListFieldMappingToDelete;
                    GetFieldsMappingWrapperList();
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Mapping saved successfully')); //Show message if the mapping is saved successfully.
                }else
                {
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please review you mapping!')); //Show message if the mapping is not saved.
                }
            }else
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'You should have at least one mapping to save!')); 
            }
            
        }
        catch(Exception ex)
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please review you mapping!'));
        }
        return null;
    }
    /*
* Description: This method is used to validate the mapping based on users current selection for dutch companies
*/    
    public List<FieldMapping__c> ValidateMapping(List<FieldsMappingWrapper> ListFieldsMappingWrapper)
    {
        List<FieldMapping__c> ListFieldMappingToUpsert = new List<FieldMapping__c>();
        try
        {
            for(FieldsMappingWrapper FieldsMappingWrapperToIterate : ListFieldsMappingWrapper)
            {
                if(FieldsMappingWrapperToIterate.SFFieldName!='' && FieldsMappingWrapperToIterate.SFFieldName!='--None--' && FieldsMappingWrapperToIterate.APIFieldName!='' && FieldsMappingWrapperToIterate.APIFieldName!='--None--' )
                {
                    //if None option is not mapped to field then add the record to the list to be updated.
                    FieldMapping__c FieldMapping = new FieldMapping__c();
                    FieldMapping.Id = FieldsMappingWrapperToIterate.FieldMapId;
                    String mappingName= selectedObject+'-'+FieldsMappingWrapperToIterate.SFFieldName.remove('creditdevice__');
                    if(mappingName.length()>38)
                    {
                        FieldMapping.Name = mappingName.substring(0,37);
                    }else
                    {
                        FieldMapping.Name = mappingName;
                    }
                    FieldMapping.Object__c = selectedObject;
                    FieldMapping.SF_Fields__c = FieldsMappingWrapperToIterate.SFFieldName;
                    FieldMapping.API_Fields__c = FieldsMappingWrapperToIterate.APIFieldName;
                    FieldMapping.Do_not_overwrite__c = FieldsMappingWrapperToIterate.doNotOverWrite;
                    ListFieldMappingToUpsert.add(FieldMapping); 
                }else if(FieldsMappingWrapperToIterate.FieldMapId!=null && (FieldsMappingWrapperToIterate.SFFieldName=='' || FieldsMappingWrapperToIterate.SFFieldName=='--None--') )
                {
                    //If mapped to none and already has mapped to other value then add the record to the list to be deleted.
                    ListFieldMappingToDelete.add(new CreditDevice__FieldMapping__c(id = FieldsMappingWrapperToIterate.FieldMapId));
                }
            } 
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        return ListFieldMappingToUpsert;
    }
    /*
* Description: This method is used to get the object list for international mapping.
*/
    public List<SelectOption> GetListOfObjectsNd()
    {
        List<SelectOption> ListObjNamesND = new List<SelectOption>();
        if(GeneralSetting!=null)
        {
            List<String> listObj = GeneralSetting.CreditDevice__Objects_Available_for_International__c.split(',');
            for(String option : listObj)
            {
                ListObjNamesND.add(new SelectOption(option,option));
            }
        }
        return ListObjNamesND;
    }
    /*
* Description: This method is used to get the already mapped/existing records from the field mapping for international mapping.
*/    
    public PageReference  GetFieldsMappingWrapperListND()
    {
        GetApiObjectFieldsND(SelectedObjectNDutch); //Get List of all Search API fields details
        ListFieldMappingToDelete.clear();
        List<String> ListexistingApiNamesNDutch = new List<String>();
        ListFieldsMappingWrapNDutch.clear();
        try{
            for(FieldMapping__c fieldMappingToIterate : FieldMapping__c.getall().values())
            {
                //if(SelectedObjectNDutch.equalsIgnoreCase(fieldMappingToIterate.Object__c) && fieldMappingToIterate.Name.contains('NDutch')){
                //v1.8 - Add International check
                if(SelectedObjectNDutch.equalsIgnoreCase(fieldMappingToIterate.Object__c) && fieldMappingToIterate.CreditDevice__International__c){
                    FieldsMappingWrapperNDutch fieldsMapWrap = new FieldsMappingWrapperNDutch(fieldMappingToIterate.Id,fieldMappingToIterate.CreditDevice__Do_not_overwrite__c,fieldMappingToIterate.SF_Fields__c.toLowerCase(),fieldMappingToIterate.API_Fields__c.toLowerCase(),MapOfApiPathtoNameNDutch.get(fieldMappingToIterate.API_Fields__c.toLowerCase()),MapOfApiPathToOrderNumberNDutch.get(fieldMappingToIterate.API_Fields__c.toLowerCase()));
                    ListFieldsMappingWrapNDutch.add(fieldsMapWrap);
                    ListexistingApiNamesNDutch.add(fieldMappingToIterate.CreditDevice__API_Fields__c);
                }
            }
            for(String apiName  : MapOfApiPathtoNameNDutch.keySet()) //Add None for every option in dropdown
            {
                if(!ListexistingApiNamesNDutch.contains(apiName))
                {
                    ListFieldsMappingWrapNDutch.add(new FieldsMappingWrapperNDutch(null,false,'--None--',apiName,MapOfApiPathtoNameNDutch.get(apiName),MapOfApiPathToOrderNumberNDutch.get(apiName.toLowerCase())));
                }
            }
            ListFieldsMappingWrapNDutch.sort();
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        return null;
    }
    /*
* Description: This method is used to get fields of selected object using Schema class
*/    
    public List<SelectOption> GetSelectedObjectFieldsND() 
    {
        Map<String, Schema.SObjectType> MapOfSchemaND = MapOfGlobalDescribeData;
        Schema.SObjectType ObjectSchemaND = MapOfSchemaND.get(SelectedObjectNDutch);
        Map<String, Schema.SObjectField> MapOfFieldND = ObjectSchemaND.getDescribe().fields.getMap();
        List<SelectOption> ListFieldNamesSelectND = new List<SelectOption>();
        ListFieldNamesSelectND.add(new SelectOption('--None--','--None--'));
        try{
            for (String fieldNameND: MapOfFieldND.keySet()) 
            {
                if(MapOfFieldND.get(fieldNameND).getDescribe().isUpdateable() && MapOfFieldND.get(fieldNameND).getDescribe().isCreateable())
                {
                    ListFieldNamesSelectND.add(new SelectOption(fieldNameND,MapOfFieldND.get(fieldNameND).getDescribe().getLabel()));
                }
            }
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        return SortSelectList(ListFieldNamesSelectND);
    }
    /*
* Description: This method is used to get fields of selected object using Schema class for international companies.
*/    
    public void GetApiObjectFieldsND(String SelectedObject)
    {
        MapOfApiPathtoNameNDutch.clear();
        MapOfApiPathToOrderNumberNDutch.clear();
        List<CreditDevice__APIFields__c > ListAllApiFields = CreditDevice__APIFields__c.getAll().values();
        ListAllApiFields.sort();
        if(!ListAllApiFields.isEmpty()){
            ListAllApiFields.sort();
            for(CreditDevice__APIFields__c field : ListAllApiFields)
            {
                if(!'Contact'.EqualsIgnoreCase(SelectedObject) && field.Is_International__c && !field.Available_for_Contact__c)
                {
                    MapOfApiPathtoNameNDutch.put(field.Path__c.toLowerCase(),field.Label__c); //Add fields for Account and Lead
                }else if('Contact'.EqualsIgnoreCase(SelectedObject) && field.Is_International__c && field.Available_for_Contact__c)
                {
                    MapOfApiPathtoNameNDutch.put(field.Path__c.toLowerCase(),field.Label__c); //Add fields for Contact
                }
                MapOfApiPathToOrderNumberNDutch.put(field.Path__c.toLowerCase(),Integer.ValueOf(field.CreditDevice__Order_Number__c));
            } 
        }
    }
    /*
* Description: This method is used to save the mapping based on users current selection for international companies
*/    
    public PageReference SaveMappingNDutch()
    {
        ListFieldMappingToDelete.clear();
        List<FieldMapping__c> ListFieldMappingToUpsertND = new List<FieldMapping__c>();
        try
        {
            if(ListFieldsMappingWrapNDutch!=null && !ListFieldsMappingWrapNDutch.isEmpty())
            {
                ListFieldMappingToUpsertND = ValidateMappingNDutch(ListFieldsMappingWrapNDutch);
                if(ListFieldMappingToUpsertND !=null && ListFieldMappingToUpsertND .Size()>0)
                {
                    upsert ListFieldMappingToUpsertND ;
                    Delete ListFieldMappingToDelete;
                    GetFieldsMappingWrapperListND();
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Mapping saved successfully'));
                    return null;
                }else
                {
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please review you mapping!'));
                }
            }else
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'You should have atlease one mapping to save!')); 
            }  
        }
        catch(Exception ex)
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Please review your mapping!'));
        }
        return null;
    }
    /*
* Description: This method is used to save the mapping based on users current selection for international companies
*/    
    public List<FieldMapping__c> ValidateMappingNDutch(List<FieldsMappingWrapperNDutch> ListFieldsMappingWrapperND)
    {
        List<FieldMapping__c> ListFieldMappingToUpsertND = new List<FieldMapping__c>();
        ListFieldMappingToDelete.clear();
        try
        {
            for(FieldsMappingWrapperNDutch FieldsMappingWrapperToIterateND : ListFieldsMappingWrapperND)
            {
                if(FieldsMappingWrapperToIterateND.SFFieldName!='' && FieldsMappingWrapperToIterateND.SFFieldName!='--None--' && FieldsMappingWrapperToIterateND.APIFieldName!='' && FieldsMappingWrapperToIterateND.APIFieldName!='--None--' )
                {
                    FieldMapping__c FieldMapping = new FieldMapping__c();
                    FieldMapping.Id = FieldsMappingWrapperToIterateND.FieldMapId;
                    //String mappingNameNDutch = SelectedObjectNDutch+'-'+FieldsMappingWrapperToIterateND.SFFieldName.remove('creditdevice__')+'NDutch';
                    //v1.8 - Add International check
                    String mappingNameNDutch = SelectedObjectNDutch+'ND-'+FieldsMappingWrapperToIterateND.SFFieldName.remove('creditdevice__');
                    FieldMapping.CreditDevice__International__c = true;
                    if(mappingNameNDutch.length()>38)
                    {
                        FieldMapping.Name = mappingNameNDutch.substring(0,37);
                    }else
                    {
                        FieldMapping.Name = mappingNameNDutch;
                    }
                    FieldMapping.Object__c = SelectedObjectNDutch;
                    FieldMapping.SF_Fields__c = FieldsMappingWrapperToIterateND.SFFieldName;
                    FieldMapping.API_Fields__c = FieldsMappingWrapperToIterateND.APIFieldName;
                    FieldMapping.Do_not_overwrite__c = FieldsMappingWrapperToIterateND.doNotOverWrite;
                    ListFieldMappingToUpsertND.add(FieldMapping);
                }else if(FieldsMappingWrapperToIterateND.FieldMapId!=null && (FieldsMappingWrapperToIterateND.SFFieldName=='' || FieldsMappingWrapperToIterateND.SFFieldName=='--None--') )
                {
                    ListFieldMappingToDelete.add(new CreditDevice__FieldMapping__c(id= FieldsMappingWrapperToIterateND.FieldMapId));
                }               
            }
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,'There is already an item in this list!'));
        }
        return ListFieldMappingToUpsertND;
    }
    /*
* Description: This inner class is used to display the Data on UI for dutch Mapping
*/
    public class FieldsMappingWrapper implements Comparable
    {
        public Id FieldMapId{get;set;}
        public Boolean doNotOverWrite{get;set;}
        public string SFFieldName{get;set;}
        public String APIFieldName{get;set;}
        public String APIFieldLabel{get;set;}
        public Integer orderNumber;
        public FieldsMappingWrapper(Id FieldMapId, Boolean doNotOverWrite, string SFFieldName, String APIFieldName, String APIFieldLabel,Integer orderNumber) 
        {
            this.FieldMapId = FieldMapId;
            this.doNotOverWrite = doNotOverWrite;
            this.SFFieldName = SFFieldName;
            this.APIFieldName = APIFieldName;
            this.APIFieldLabel = APIFieldLabel;
            this.orderNumber = orderNumber;
        }
        public Integer compareTo(Object ObjToCompare)
        {
            FieldsMappingWrapper fieldMappingWrap= (FieldsMappingWrapper)ObjToCompare;
            Integer returnValue = 0;
            If(orderNumber > fieldMappingWrap.orderNumber)
            {
                returnValue = 1;
            }else if(orderNumber < fieldMappingWrap.orderNumber)
            {
                returnValue = -1;
            }
            return returnValue;
        } 
    }
    /*
* Description: This inner class is used to display the Data on UI for international Mapping
*/
    public class FieldsMappingWrapperNDutch implements Comparable
    {
        public Id FieldMapId{get;set;}
        public Boolean doNotOverWrite{get;set;}
        public string SFFieldName{get;set;}
        public String APIFieldName{get;set;}
        public String APIFieldLabel{get;set;}
        public Integer orderNumber;
        public FieldsMappingWrapperNDutch(Id FieldMapId, Boolean doNotOverWrite, string SFFieldName, String APIFieldName,String APIFieldLabel,Integer orderNumber) 
        {
            this.FieldMapId = FieldMapId;
            this.doNotOverWrite = doNotOverWrite;
            this.SFFieldName = SFFieldName;
            this.APIFieldName = APIFieldName;
            this.APIFieldLabel =APIFieldLabel;
            this.orderNumber = orderNumber;
        }
        public Integer compareTo(Object ObjToCompare)
        {
            FieldsMappingWrapperNDutch fieldMappingWrapND= (FieldsMappingWrapperNDutch)ObjToCompare;
            Integer returnValue = 0;
            If(orderNumber > fieldMappingWrapND.orderNumber)
            {
                returnValue = 1;
            }else if(orderNumber < fieldMappingWrapND.orderNumber)
            {
                returnValue = -1;
            }
            return returnValue;
        } 
    } 
}