/***************************************************************
    Class : ReportViewController
    Developer : Hetal Shukla
    Created Date : 03/08/2019
    Summary : This class is used to fetch and display the PDF credit report
    Change Log: 
    v1.0 - 03/08/2019 - HetalS - Created this new controller.(82% Code Coverage)
   *****************************************************************/

public class ReportViewController {   
    public String InquiryId{get;set;}
    public ReportViewController()
    {     
        InquiryId =  ApexPages.currentPage().getParameters().get('id');       
    }  
    /*
    This method is used to call the API to fetch the PDF report and display on VF page
    */
    public String getReportData() {
        try
        {
            if(InquiryId == null)
            {
                Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, 'Error: Please first order report and proceed.'));    
                return null;
            }
            HttpRequest request = new HttpRequest(); 
            Http httpObj = new Http();
            string apiName = 'inquiries/'+InquiryId+'/report?format=PDF';            
            request = Util.GetGeneratedRequest(null,apiName,null ,'GET');         
          	request.setHeader('content-type', 'application/pdf');
            request.setTimeout(120000);
            HttpResponse pdfResponse = httpObj.send(request);
            if (pdfResponse.getStatusCode() == 200) 
            {               
               	Blob resBlob = pdfResponse.getBodyAsBlob();               
                String pdfContent = EncodingUtil.base64Encode(resBlob);                
                return  pdfContent;  
            }
            else
            {
                Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, 'Error: '+pdfResponse.getStatus()));    
            }
        }
        catch(Exception e)
        {           
            {
                Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, 'Error: '+e.getMessage()));    
            }
        }
        return null;
    }
}