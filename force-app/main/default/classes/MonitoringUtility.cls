/***************************************************************
Class : MonitoringUtility
Developer : Sneha U.
Created Date : 03/25/19
Summary : This class includes utility methods for Monitoring callouts & process
Change Log: 
v1.0 - 03/25/2019 - Sneha U. - Initial Creation.(86% Code Coverage)
v1.1 - 05/06/2019 - Sneha U. - Add Non Dutch checkbox field check instead of NDutch string
v1.2 - 05/16/2019 - Kalpana G. - Postal Code formatting for dutch companies. 
v1.3 - 05/24/2019 - Sneha U. - Branch Companies to be populated with respective paths
v1.4 - 05/24/2019 - Sneha U. - add zipcode for branch from the details in relationship
v1.5 - 06/12/2019 - Kalpana G. - bug fix for CreditDevice id truncate
v1.6 - 07/12/2019 - Sneha U. - Get Alert Types from Custom Settings and process respectively
v1.7 - 07/26/2019 - Sneha U. - Insert Alerts instead of updating Account
v1.8 - 08/08/2019 - Sneha U. - If branch is inactive(details not found in instant api) then dont populate values with random branch
v1.9 - 03/05/2020 - Sneha U. - Fix the endpoint for getting alerts (year=)
*************************************************************************************************************************/
public class MonitoringUtility
{
    public static Map<String,Schema.SObjectType> MapOfGlobalDescribeData = Schema.getGlobalDescribe();
    public static String DutchCountryCode
    {
        get{
            if(DutchCountryCode == null)
            {
                //Get Dutch country code from custom settings
                if(generalSetting!=null)
                {
                    DutchCountryCode = generalSetting.Dutch_Country_Code__c;
                }
            }
            return DutchCountryCode;
        }
        set;
    }
    public static Credit_Device_General_Setting__c generalSetting
    {
        get{
            if(generalSetting == null)
            {
                //Get general settings from custom settings
                generalSetting = Credit_Device_General_Setting__c.getValues('GeneralSetting');
            }
            return generalSetting;
        }
        set;
    }    
    public static Map<String, Schema.SObjectField> MapOfObjectFields;
    public static Map<String, Schema.DisplayType> MapOfObjectFieldType = new Map<String, Schema.DisplayType>();
    public static List<CreditDevice__FieldMapping__c> ListFieldMapping = CreditDevice__FieldMapping__c.getall().values();
    public static List<CreditDevice__APIFields__c> ListAPIFields = CreditDevice__APIFields__c.getall().values();
    public static Map<String,String> MapSfFieldsToApiField;
    public static String CurrentFieldName;

    //v1.3 Construct to get the Head Quarter path and branch path
    public static Map<String,String> MapHQPathBranchPath 
    {
        get{
            if(MapHQPathBranchPath == null)
            {
                MapHQPathBranchPath=new Map<String,String>();

                for(CreditDevice__APIFields__c apiField:ListAPIFields){
                    if(apiField.Branch_Path__c!=null && apiField.Branch_Path__c!=''){
                        MapHQPathBranchPath.put(apiField.path__c,apiField.Branch_Path__c);
                    }
                }
            }
            return MapHQPathBranchPath;
        }
        set;
    }  
    //v1.3 end

    //Callout to add a Instant search
    public static HttpResponse instantReport(String endpoint,String regNum)
    {
        Http httpEntry = new Http();
        Map<String,String> mapRequestHeader = new Map<String,String>();
        mapRequestHeader.put('Content-Type','application/json');
        //Create request body
        String requestBody = '{ "reg_number":"'+regNum+'"}';
        HttpRequest request = Util.GetGeneratedRequest(mapRequestHeader,endpoint,requestBody,'POST'); //Generate HTTP request
        //Send request to activate account for monitoring
        HttpResponse response = httpEntry.send(request);
        return response;
    }
    //Callout to add a Monitoring Entry
    public static HttpResponse addMonitoringEntry(String endpoint)
    {
        Http httpEntry = new Http();
        Map<String,String> mapRequestHeader = new Map<String,String>();
        mapRequestHeader.put('Content-Type','application/json');
        //Create request body
        String requestBody = '{ "continue":true}';
        HttpRequest request = Util.GetGeneratedRequest(mapRequestHeader,endpoint,requestBody,'PUT'); //Generate HTTP request
        //Send request to activate account for monitoring
        HttpResponse response = httpEntry.send(request);
        return response;
    }
    //Callout to browse alerts
    public static HttpResponse browseAlert(String endpoint)
    {
        //Generate request
        HttpRequest request = Util.GetGeneratedRequest(new Map<String,String>(),endpoint,null,'GET');
        //Send request to activate account for monitoring
        Http httpAlert = new Http();
        HttpResponse response = httpAlert.send(request);
        return response;
    }
    //Process Monitoring entries
    public static Boolean processMonitoringEnrty(String endpoint)
    {
        Boolean isMonitored=false;
        HttpResponse response = MonitoringUtility.addMonitoringEntry(endpoint);  
        //process Response
        if (response.getStatusCode() == 200) 
        {
            isMonitored=true;  
        }
        return isMonitored;
    }
    //Process alerts and mark accounts as alerts exist for Monitoring
    public static void processAlerts(Date alertDate)
    {

        //Get the Alert types from custom settings
        Set<String> setHQType = new Set<String>();
        Set<String> setAllType = new Set<String>();

        //Get HQ alert types
        if(generalSetting.CreditDevice__Alert_Type_HQ__c!=null && generalSetting.CreditDevice__Alert_Type_HQ__c!=''){
            for(String typeAlert: generalSetting.CreditDevice__Alert_Type_HQ__c.split(',')){
                setHQType.add(typeAlert.trim());
            }
        }

        //Get ALL alert types
        if(generalSetting.CreditDevice__Alert_Type_ALL__c!=null && generalSetting.CreditDevice__Alert_Type_ALL__c!=''){
            for(String typeAlert: generalSetting.CreditDevice__Alert_Type_ALL__c.split(',')){
                setAllType.add(typeAlert.trim().toUpperCase());
            }
        }

        Set<String> setRegNumberForAlerts = new Set<String>();
        Set<String> setRegNumberForCreditLimit = new Set<String>();

        Set<String> setRegNumberForAllAlerts = new Set<String>();
        Set<String> setRegNumberForHQAlerts = new Set<String>();
        
        boolean more_items=true;
        integer calloutCount = 1;
        //callout response page number 1
        Integer current_page=1;
        //endpoint url to include todays date 
        String endpointMain ='monitor/alerts?year='+alertDate.year()+'&month='+alertDate.month()+'&day='+alertDate.day()+'&limit=100';

        //Process until more items=false
        while(more_items & calloutCount<=100)
        {
            String endpoint=endpointMain+'&page='+current_page;
            HttpResponse alertResponse = browseAlert(endpoint);    //browse alert to get which company details are updated.
            calloutCount++; //update count of callout by 1

            //processResponse
            if (alertResponse.getStatusCode() == 200) 
            {
                String alertBody = alertResponse.getBody().replace('list', 'list1').replace('type','type1');
                AlertWrapper alertResponseObj = (AlertWrapper) System.JSON.deserialize(alertBody, AlertWrapper.class);   
        
                if(alertResponseObj!=null)
                {
                    if(alertResponseObj.meta!=null)
                    {
                        more_items = alertResponseObj.meta.more_items;
                        current_page++;
                    }
                    if(alertResponseObj.list1!=null)
                    {
                        for(AlertListWrapper objAlert:alertResponseObj.list1)
                        {
                            //If HQ and branch both to be updated
                            if(setAllType.contains(objAlert.type1.toUpperCase()))
                            {
                                //If type other then credit limit change
                                setRegNumberForAllAlerts.add(objAlert.reg_number);
                            } 
                            //If only HQ needs to be updated for this alert type
                            else if(setHQType.contains(objAlert.type1.toUpperCase()))
                            {
                                //If type other then credit limit change
                                setRegNumberForHQAlerts.add(objAlert.reg_number);
                            }

                            //Check if there is a creditlimit change
                            if(objAlert.type1.equalsIgnoreCase('REVIEW_INCREASE') || objAlert.type1.equalsIgnoreCase('REVIEW_DECREASE')  )
                            {
                                setRegNumberForCreditLimit.add(objAlert.reg_number);
                            }

                             
                        }
                    }
                }
            }
        }
        //v1.7 Use alerts to store account ids with alerts
        List<CreditDevice__Alert__c> listAlert = new List<CreditDevice__Alert__c>();

        //delete existing alerts
        List<CreditDevice__Alert__c> listAlertsToDelete = new List<CreditDevice__Alert__c>();
        listAlertsToDelete = [select id from CreditDevice__Alert__c];

        if(listAlertsToDelete!=null && listAlertsToDelete.size()>0){
            delete listAlertsToDelete;
        }

        //get all accounts with respective registration number
        List<Account> listAccount = [select id,CreditDevice__Credit_Limit_Change__c,CreditDevice__Registration_Number__c,CreditDevice__EST_Number__c,CreditDevice__Head_Quarter__c from Account where CreditDevice__Company_Id__c!=null 
                                     and CreditDevice__Is_International__c=false and (CreditDevice__Registration_Number__c in :setRegNumberForAllAlerts or CreditDevice__Registration_Number__c in :setRegNumberForHQAlerts or CreditDevice__Registration_Number__c in:setRegNumberForCreditLimit)];

        //Check accounts exist & update
        if(listAccount!=null && listAccount.size()>0)
        {
            for(Account objAccount: listAccount)
            {
                Boolean isUpdate =false;
                Boolean isCreditChange =false;

                //Check if alerts exist for all
                if(setRegNumberForAllAlerts.contains(objAccount.CreditDevice__Registration_Number__c))
                {
                    isUpdate = true;
                }

                //Check if alerts exist for only HQ
                else if(objAccount.CreditDevice__Head_Quarter__c && setRegNumberForHQAlerts.contains(objAccount.CreditDevice__Registration_Number__c))
                {
                    //objAccount.CreditDevice__Alerts_Exist__c=true;
                    isUpdate = true;
                }
                
                //check if credit limit change exist
                if(setRegNumberForCreditLimit.contains(objAccount.CreditDevice__Registration_Number__c))
                {

                    isCreditChange = true;
                    isUpdate = true;
                }

                if(isUpdate){
                    listAlert.add(new CreditDevice__Alert__c(CreditDevice__Account_Id__c=objAccount.id,CreditDevice__Credit_Limit_Change__c=isCreditChange));
                }
            }
        }

        //get all Leads with respective registration number
        List<Lead> listLead = [select id,CreditDevice__Registration_Number__c,CreditDevice__Credit_Limit_Change__c,CreditDevice__EST_Number__c,CreditDevice__Head_Quarter__c from Lead where CreditDevice__Company_Id__c!=null 
                               and CreditDevice__Is_International__c=false and (CreditDevice__Registration_Number__c in :setRegNumberForAllAlerts or CreditDevice__Registration_Number__c in :setRegNumberForHQAlerts or CreditDevice__Registration_Number__c in:setRegNumberForCreditLimit)];
        //List<Lead> listLeadToUpdate = new List<Lead>();

        //Check leads exist & update
        if(listLead!=null && listLead.size()>0)
        {
            for(Lead objLead: listLead)
            {
                Boolean isUpdate =false;
                Boolean isCreditChange =false;

                //Check if alerts exist for all
                if(setRegNumberForAllAlerts.contains(objLead.CreditDevice__Registration_Number__c))
                {
                    isUpdate = true;
                }

                //Check if alerts exist for only HQ
                else if(objLead.CreditDevice__Head_Quarter__c && setRegNumberForHQAlerts.contains(objLead.CreditDevice__Registration_Number__c))
                {
                    isUpdate = true;
                }
                
                //check if credit limit change exist
                if(setRegNumberForCreditLimit.contains(objLead.CreditDevice__Registration_Number__c))
                {
                    isUpdate = true;
                    isCreditChange=true;
                }

                if(isUpdate){
                    listAlert.add(new CreditDevice__Alert__c(CreditDevice__Lead_Id__c=objLead.id,CreditDevice__Credit_Limit_Change__c=isCreditChange));
                }
            }

        }

        //Update Alerts if exist
        if(listAlert.size()>0){
            List<Database.SaveResult> listUpdatedAlerts = database.insert(listAlert,false);

            Integer batchsize = 10;
            if(generalSetting.CreditDevice__Monitoring_Batch_Size__c!=null && generalSetting.CreditDevice__Monitoring_Batch_Size__c!=0){
                batchsize=Integer.valueOf(generalSetting.CreditDevice__Monitoring_Batch_Size__c);
            }

            if(!Test.isRunningTest()){
                //Execute next batch for instant update
                Database.executeBatch(new MonitoringInstantUpdateBatch('Account'),batchsize);
            }
        }
        
    }
    //Monitor using Intant reports
    public static SObject monitorWithInstantReport(Boolean isHeadQuarter,String currentESTNumber ,String regNum,String recordObjectName)
    {
        String endpoint = 'instants';
        Map<String,String> MapOfAccessibleSfFieldsToApiField = new Map<String,String>();
        Map<String, Object> mapMeta = new Map<String, Object>();
        SObject recordToUpdate;
        //Get the instant report response
        HttpResponse response = instantReport(endpoint,regNum);
        //Process reponse
        if(response!=null && response.getStatusCode() == 200)
        {
            mapMeta = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        }
        //Check if field mapping exists
        if(MapSfFieldsToApiField==null)
        {
            getFieldMapping(recordObjectName); 
        }
        //Get Field map for respective object
        if(MapOfObjectFields==null)
        {
            MapOfObjectFields = MapOfGlobalDescribeData.get(recordObjectName).getDescribe().fields.getMap();
        }
        MapOfAccessibleSfFieldsToApiField = Util.getAccessibleFields(MapOfGlobalDescribeData.get(recordObjectName).newSObject(),'Update',MapSfFieldsToApiField); //Get Accessible fields for Insert operation
        //Parse and populate fields with value from response
        recordToUpdate = populateObjectFields(isHeadQuarter,currentESTNumber, mapMeta,recordObjectName,MapOfAccessibleSfFieldsToApiField,MapOfObjectFields);
        return recordToUpdate;
    }
    //Get mapping for respective field
    public static void getFieldMapping(String recordObjectName)
    {
        MapSfFieldsToApiField = new Map<String,String>();
        //Get mapping for only Dutch
        for(CreditDevice__FieldMapping__c fieldMapping:ListFieldMapping)
        {
            //v1.1 - Add International check
            //if( !fieldMapping.CreditDevice__Do_not_overwrite__c && !fieldMapping.Name.contains('NDutch'))
            if( !fieldMapping.CreditDevice__Do_not_overwrite__c && !fieldMapping.CreditDevice__International__c )
            {
                if(recordObjectName.equalsIgnoreCase(fieldMapping.CreditDevice__Object__c))
                {
                    MapSfFieldsToApiField.put(fieldMapping.CreditDevice__SF_Fields__c.toLowerCase(),fieldMapping.CreditDevice__API_Fields__c.toLowerCase());
                }
            }   
        }
    }
    
    //Populate fields from mapping in sObject
    public static SObject populateObjectFields(Boolean isHeadQuarter, String myEst , Map<String,Object> meta,String objectName, Map<String,String> mapSFApiFields,Map<String, Schema.SObjectField> mapofObjectField)
    {
        Sobject objCompany = MapOfGlobalDescribeData.get(objectName).newSObject() ;
        //If there exists mapping for respective Object
        if(!mapSFApiFields.isEmpty() && !meta.isEmpty() && !mapofObjectField.isEmpty())
        {

            //v1.3 If company is branch the select mapping for the same
            if( objectName.equalsIgnoreCase('Account') || objectName.equalsIgnoreCase('Lead') ){
                if(!isHeadQuarter ){
                    for(String field : mapSFApiFields.keySet())
                    {
                        if(MapHQPathBranchPath.containsKey(mapSFApiFields.get(field) )){
                            mapSFApiFields.put(field,MapHQPathBranchPath.get(mapSFApiFields.get(field)));
                        }
                    }
                    if(meta.containsKey('relations'))
                    {
                        Map<String,Object> mapRelations = (Map<String,Object>)meta.get('relations');
                        if(mapRelations.containsKey('branches'))
                        {
                            List<Object> listObject = (List<Object>) mapRelations.get('branches');
                            Boolean branchExists =false;
                            for(Object branch:listObject){
                                Map<String,Object> mapbranch = (Map<String,Object>)branch;
                                if(mapbranch.containsKey('branch_number'))
                                {
                                    string branchnumber = String.valueOf(mapbranch.get('branch_number'));
                                    if(myEst==branchnumber){
                                        branchExists = true;
                                        List<Object> listObject1 = new List<Object>();
                                        listObject1.add(branch);
                                        mapRelations.put('branches',(Object) listObject1);
                                        Object obj = (Object) mapRelations;
                                        
                                        meta.put('relations',obj);
                                    }
                                }
                            }

                            //if branch does not exist then remove branch from meta
                            if(myEst!='' && myEst!=null && !branchExists){
                                List<Object> listObject1 = new List<Object>();
                                mapRelations.put('branches',(Object) listObject1);
                                Object obj = (Object) mapRelations;
                                meta.put('relations',obj);
                            }
                        }
                    }
                }
                //Update whether its head quarter or not
                objCompany.put('CreditDevice__Head_Quarter__c',isHeadQuarter);
            }
            //v1.3 end

            //For each mapping
            for(String field : mapSFApiFields.keySet())
            {
                //used to get the field info for error message
                CurrentFieldName = field;
                //Get path list for specific field
                List<String> listPath = mapSFApiFields.get(field).split('/');
                if(listPath.size()>0)
                {
                    Map<String,Object> mapObjInfo = new Map<String,Object>();
                    mapObjInfo.putAll(meta);
                    Map<String,Map<String,Object>> mapPathObject = new Map<String,Map<String,Object>>();
                    for(Integer i=0;i<listPath.size();i++)
                    {
                        //Get the whole path list for saving map key
                        String pathStr = '';
                        for(Integer j=0;j<=i;j++)
                        {
                            pathStr+=listPath[j]+'/';
                        }
                        pathStr = pathStr.removeEnd('/');
                        //If the response is not a list
                        if(!listPath[i].contains('[]') && mapObjInfo.containsKey(listPath[i]))
                        {
                            if(mapObjInfo.get(listPath[i]) instanceof String || mapObjInfo.get(listPath[i]) instanceof Boolean)
                            {
                                mapPathObject.put(pathStr,new Map<String,Object>{listPath[i]=>mapObjInfo.get(listPath[i])});
                            }else
                            {
                                Map<String,Object> mapKeyInfo = (Map<String,Object>)mapObjInfo.get(listPath[i]);
                                mapObjInfo.putAll(mapKeyInfo);
                                mapPathObject.put(pathStr,mapKeyInfo);
                            }
                          
                        }else
                        {

                            //If the response is a list
                            if(mapObjInfo.containsKey(listPath[i].remove('[]')))
                            {
                                if(listPath[i]=='branches[]'){
                                    Map<String,Object> map222 = new Map<String,Object>();

                                    //Null check for relations
                                    if(mapObjInfo.get('relations')!=null){
                                        map222 = (Map<String,Object>)mapObjInfo.get('relations');
                                    }

                                    List<Object> listObjMap = new List<Object>();

                                    //Null check for branches
                                    if(map222.get('branches')!=null){
                                        listObjMap = (List<Object>) map222.get('branches');
                                    }

                                    Map<String,Object> mapMyObj = new Map<String,Object>();

                                    //Null check for branches list
                                    if(listObjMap!=null && listObjMap.size()>0){
                                        mapMyObj = (Map<String,Object>)listObjMap[0];
                                    }
                                    
                                    for(String key: mapMyObj.keyset()){
                                        mapPathObject.put(pathStr+'/'+key,new Map<String,Object>{key=>mapMyObj.get(key)});
                                    }
                                }
                                String infoString = String.valueOf(mapObjInfo.get(listPath[i].remove('[]')));
                                List<String> listKeyInfo = infoString.split(',');
                                if(infoString.contains('='))
                                {
                                    //Process each item in the list
                                    for(Integer k=0;k<listKeyInfo.size();k++)
                                    {
                                        String newStringValue = String.valueOf(listKeyInfo[k].replaceAll('\\p{Ps}','').replaceAll('\\p{Pe}','') );
                                        List<String> listInnerKeyInfo = newStringValue.split(',');
                                        //If list has records
                                        if(listInnerKeyInfo.size()>0)
                                        {
                                            if(listInnerKeyInfo[0].contains('=') && !listInnerKeyInfo[0].endsWith('='))
                                            {                                     
                                                String newInnerStringValue = listInnerKeyInfo[0].split('=')[1].replaceAll('\\p{Ps}','').replaceAll('\\p{Pe}','');
                                                mapObjInfo.put(listInnerKeyInfo[0].split('=')[0].deleteWhitespace(),newInnerStringValue);
                                                mapPathObject.put(pathStr,new Map<String,Object>{listInnerKeyInfo[0].split('=')[0].deleteWhitespace()=>newInnerStringValue});
                                            }
                                            else if(listInnerKeyInfo[0].contains('=') && listInnerKeyInfo[0].endsWith('='))
                                            {                                  
                                                String newInnerStringValue = listInnerKeyInfo[0].replaceFirst('=','#').split('#')[1].replaceAll('\\p{Ps}','').replaceAll('\\p{Pe}','');
                                                mapObjInfo.put(listInnerKeyInfo[0].split('=')[0].deleteWhitespace(),newInnerStringValue);
                                                mapPathObject.put(pathStr,new Map<String,Object>{listInnerKeyInfo[0].split('=')[0].deleteWhitespace()=>newInnerStringValue});
                                            }
                                        }
                                    }   
                                }else if(listKeyInfo.size()>0)
                                {
                                    mapPathObject.put(pathStr,new Map<String,Object>{listPath[i].remove('[]')=>listKeyInfo[0]});
                                }
                            }
                        }
                    }
                    Schema.DisplayType valueType;
                    //Check if field describe already exists
                    if(MapOfObjectFieldType.containsKey(field))
                    {
                        valueType = MapOfObjectFieldType.get(field);
                    }else
                    {
                        //If not describe to get the type
                        valueType = mapofObjectField.get(field).getDescribe().getType();
                        MapOfObjectFieldType.put(field,valueType);
                    }
                    //If the data type is not string
                    if(mapPathObject.get(mapSFApiFields.get(field))!=null && mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1]) instanceof String && valueType != Schema.DisplayType.String && valueType!= Schema.DisplayType.TextArea)
                    {
                        //Update value as per the data type Update field as per the data type
                        //if type equals Date Type
                        if(valueType == Schema.DisplayType.Date)
                        {
                            List<String> listDate = String.ValueOf(mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1])).replaceAll('}','').split('-');
                            if(!listDate.isEmpty())
                            {
                                objCompany.put(field,date.newinstance(Integer.valueOf(listDate[0]), Integer.valueOf(listDate[1]),Integer.valueOf(listDate[2]))); 
                            }
                        }
                        //if type equals Currency
                        else if(valueType == Schema.DisplayType.Currency)
                        {
                            String str = String.Valueof(mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1])).replaceAll('[a-zA-Z]','');
                            objCompany.put(field,Decimal.valueOf(str));
                        }
                        //if type equals Boolean
                        else if(valueType == Schema.DisplayType.Boolean)
                        {
                            objCompany.put(field,Boolean.valueOf(listPath[listPath.size()-1]));
                        }
                        //if type equals Percent
                        else if(valueType == Schema.DisplayType.Percent)
                        {
                            objCompany.put(field,Decimal.valueOf(String.ValueOf(mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1]))));
                        }
                        //if type equals Double
                        else if(valueType == Schema.DisplayType.Double)
                        {
                            objCompany.put(field,Double.valueOf(mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1]))); 
                        }
                        //if type equals Integer
                        else if(valueType == Schema.DisplayType.Integer)
                        {
                            objCompany.put(field,Integer.valueOf(mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1]))); 
                        }
                        //If its a list
                        else
                        {
                            if(listPath[listPath.size()-1].contains('[]') && mapPathObject.get(mapSFApiFields.get(field)).containsKey(listPath[listPath.size()-1].remove('[]')))
                            {
                                List<String> listOfKeyInfo = String.valueOf(mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1].remove('[]'))).split(',');
                                if(listOfKeyInfo.size()>0)
                                {
                                    mapPathObject.get(mapSFApiFields.get(field)).put(listPath[listPath.size()-1].remove('[]').deleteWhitespace(),listOfKeyInfo[0].replaceAll('\\p{Pe}','').replaceAll('\\p{Ps}',''));
                                    objCompany.put(field,mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1].remove('[]')));
                                }
                            }
                            else
                            {
                                if(mapPathObject.get(mapSFApiFields.get(field)).containsKey(listPath[listPath.size()-1]))
                                {
                                    objCompany.put(field,mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1]));
                                }
                            }
                        }
                    }
                    //If the data type is String
                    else
                    {
                        //If the type is List
                        if(listPath[listPath.size()-1].contains('[]') && mapPathObject.containsKey(mapSFApiFields.get(field)) && mapPathObject.get(mapSFApiFields.get(field)).containsKey(listPath[listPath.size()-1].remove('[]')))
                        {
                            String infoString = String.valueOf(mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1].remove('[]')) );
                            List<String> listOfKeyInfo = infoString.split(',');
                            if(listOfKeyInfo.size()>0)
                            {
                                mapPathObject.get(mapSFApiFields.get(field)).put(listPath[listPath.size()-1].remove('[]').deleteWhitespace(),listOfKeyInfo[0].replaceAll('\\p{Pe}','').replaceAll('\\p{Ps}',''));
                                objCompany.put(field,mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1].remove('[]')));
                            }
                        }
                        else
                        {
                            //Check if the field value exists and populate
                            if(mapPathObject.containsKey(mapSFApiFields.get(field)) && mapPathObject.get(mapSFApiFields.get(field)).containsKey(listPath[listPath.size()-1]))
                            {
                                objCompany.put(field,mapPathObject.get(mapSFApiFields.get(field)).get(listPath[listPath.size()-1]));
                            }
                        }
                    }
                }
                
            }
            
        }
        //Format the postal code for dutch company
        String PostalCodeFormat = generalSetting.CreditDevice__Dutch_Postal_Code_Format__c;
        if(!String.isBlank(PostalCodeFormat) && generalSetting!=null && PostalCodeFormat.containsWhitespace())
        {
            if(objCompany instanceof Account)
            {
                String AccountPostalCodeFields = String.ValueOf(generalSetting.CreditDevice__Account_Postal_Code_Fields_To_Format__c).toLowerCase();
                if(!String.isBlank(AccountPostalCodeFields))
                {
                    for(String postalCodeField : AccountPostalCodeFields.split(','))
                    {
                        String FormattedPostalCode = '';
                        if(!String.isBlank((String)objCompany.get(postalCodeField)) && mapSFApiFields.containskey(postalCodeField))
                        {
                            for(String postCode :String.ValueOf(objCompany.get(postalCodeField)).splitByCharacterType())
                            {
                                FormattedPostalCode +=postCode + ' ';
                            }
                            FormattedPostalCode.trim();
                            objCompany.put(postalCodeField,FormattedPostalCode);
                        }
                    }
                }
            }else if(objCompany instanceof Lead)
            {
                String LeadPostalCodeFields = generalSetting.CreditDevice__Lead_Postal_Code_Fields_To_Format__c.toLowerCase();
                if(!String.isBlank(LeadPostalCodeFields))
                {
                    for(String postalCodeField : LeadPostalCodeFields.split(','))
                    {
                        String FormattedPostalCode = '';
                        if(!String.isBlank((String)objCompany.get(postalCodeField)) && mapSFApiFields.containskey(postalCodeField))
                        {
                            for(String postCode :String.ValueOf(objCompany.get(postalCodeField)).splitByCharacterType())
                            {
                                FormattedPostalCode +=postCode + ' ';
                            }
                            FormattedPostalCode.trim();
                            objCompany.put(postalCodeField,FormattedPostalCode);
                        }
                    }
                }
                
            }
        }
        return objCompany;
    }
    
    //Wrapper for alert report - begin
    public class AlertWrapper
    {
        public List<AlertListWrapper> list1;
        public AlertMetaWrapper meta;
    }
    public class AlertListWrapper
    {
        String id;
        String reg_number;
        String reference;
        String type1;
        String created_at;
        AlertReference refs;
    }
    public class AlertMetaWrapper
    {
        public Integer items;
        public Integer current_page;
        public boolean more_items;
    }
    public class AlertReference
    {
        public String self;
        public String report;
    }
    //Wrapper for alert report - end
}