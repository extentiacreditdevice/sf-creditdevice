@isTest
public class TestCreditDeviceDecoupleController
{
    @testSetup static void setup() {
        Credit_Device_Custom_Setting__c creds = new Credit_Device_Custom_Setting__c();
        creds.Name = 'CreditDeviceCreds';
        creds.Username__c = 'Test';
        creds.Password__c ='Test@1234';
        creds.Endpoint_URL__c = 'https://inquiry.creditandcollection.nl/api/';
        insert creds;
        Credit_Device_General_Setting__c generalSetting = new Credit_Device_General_Setting__c();
        generalSetting.Name = 'GeneralSetting';
        generalSetting.Disable_Lead__c = false;
        generalSetting.Dutch_Country_Code__c = 'NL';
        generalSetting.Dutch_Language_Code__c ='NL';
        generalSetting.International_Product_Code__c = '701';
        generalSetting.National_Product_Code__c ='702';
        generalSetting.Non_Dutch_Language_Code__c ='EN';
        generalSetting.CreditDevice__Alert_Type_ALL__c ='INSOLVENCY,REVIEW_INCREASE,REVIEW_DECREASE,TERMINATION';
        generalSetting.CreditDevice__Alert_Type_HQ__c ='NAW';
        insert generalSetting;
           
        List<CreditDevice__Country__c> countryList = new List<CreditDevice__Country__c>();
        CreditDevice__Country__c country1 = new CreditDevice__Country__c();
        country1.CreditDevice__Country_Code__c ='NL';
        country1.Name = 'Netherlands';
        countryList.add(country1);
        
        CreditDevice__Country__c country2 = new CreditDevice__Country__c();
        country2.CreditDevice__Country_Code__c ='IN';
        country2.Name = 'India';
        countryList.add(country2);
        insert countryList;
        
        Account acc = new Account();
        acc.Name= 'Test account';
        acc.BillingCity   = 'Amsterdam';
        acc.CreditDevice__Company_Id__c ='l5eG0lBqUSUTshaAs4cioA';
        acc.CreditDevice__Is_International__c=true;
        acc.BillingPostalCode  ='1000AA';
        acc.BillingCountry='US';
        acc.CreditDevice__Registration_Number__c= '1234567HG';
        insert acc;
        
        Lead ld = new Lead();
        ld.LastName = 'Test Lead11';
        ld.Company = 'Test Company';
        ld.Country = 'NL';
        ld.Registration_Number__c = '12345678';
        ld.Company_Id__c ='l5eG0lBqABCDshaAs4cioA';
        ld.city = 'Amsterdam';
        ld.PostalCode ='1000AA';
        insert ld;
        
    }
    public testMethod static void TestdecoupleRecordforLead()
    {
        Lead ld = [SELECT id from Lead LIMIT 1];
        System.assertNotEquals(null,ld);
        PageReference pageRef = Page.CreditDeviceDecouplePage;
        Test.setCurrentPage(pageRef);
        // put the lead id as a parameter
        ApexPages.currentPage().getParameters().put('id',ld.id);
        // Call your apex method here and you will get code coverage
        Test.startTest();
        CreditDeviceDecoupleController decoupleCtrl = new CreditDeviceDecoupleController();
        System.assertEquals('Lead',decoupleCtrl.RecordObjectName);
        pageReference pageRef1 = decoupleCtrl.decoupleRecord();
        Test.stopTest();
    }
    public testMethod static void TestdecoupleRecordforAccount()
    {
        Account acc = [SELECT id from Account LIMIT 1];
        System.assertNotEquals(null,acc);
        PageReference pageRef = Page.CreditDeviceDecouplePage;
        Test.setCurrentPage(pageRef);
        // put the lead id as a parameter
        ApexPages.currentPage().getParameters().put('id',acc.id);
        // Call your apex method here and you will get code coverage
        Test.startTest();
        CreditDeviceDecoupleController decoupleCtrl = new CreditDeviceDecoupleController();
        System.assertEquals('Account',decoupleCtrl.RecordObjectName);
        pageReference pageRef1 = decoupleCtrl.decoupleRecord();
        Test.stopTest();
    }
    public testMethod static void TestNotConnectedAccount()
    {
        Account acc = [SELECT id,Company_Id__c from Account LIMIT 1];
        System.assertNotEquals(null,acc);
        acc.Company_Id__c ='';
        Test.startTest();
        update acc;
        PageReference pageRef = Page.CreditDeviceDecouplePage;
        Test.setCurrentPage(pageRef);
        // put the lead id as a parameter
        ApexPages.currentPage().getParameters().put('id',acc.id);
        // Call your apex method here and you will get code coverage
        CreditDeviceDecoupleController decoupleCtrl = new CreditDeviceDecoupleController();
        System.assertEquals(null,decoupleCtrl.CompanyId);
        System.assertEquals('Account',decoupleCtrl.RecordObjectName);
        pageReference pageRef1 = decoupleCtrl.decoupleRecord();
        Test.stopTest();
    } 
}