/***************************************************************
    Class : OrderReportExtension
    Developer : RahulG
    Created Date : 02/25/2019
    Summary : This class is used to check the permission set 
    Change Log: 
    v1.0 - 02/25/2019 - RahulG - Created this new controller.(93% Code Coverage)
    v1.1 - 03/08/2019 - HetalS - Modified controller to make it generic for both Account and Lead, created all the wrapper classes
    v1.2 - 03/15/2019 - HetalS - The code has been move to OrderReportController because of the lightning componnent approach
    v1.3 - 03/22/2019 - HetalS - Added the code to check the permissionset
   *****************************************************************/
public with sharing class OrderReportExtension 
{
    public boolean hasPermission {get;set;}
    public OrderReportExtension(ApexPages.StandardController stdController){
        List<string> listpermissionSetNames = new List<String> {'CreditDevice_Admin','CreditDevice_User'};
        hasPermission = Util.hasPermission(listpermissionSetNames);
        if(!hasPermission)
        {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, System.Label.MSG_NO_ACCESS_TO_SECTION));
        }
    }
}