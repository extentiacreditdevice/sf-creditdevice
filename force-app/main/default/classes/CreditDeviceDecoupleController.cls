/***************************************************************
Class : CreditDeviceDecoupleController
Developer : Kalpana Gosavi
Created Date : 19/04/2019
Details : This class is used to develope decouple functionality for CreditDevice.
Test Coverage Classes: TestCreditDeviceDecoupleController
Change Log : 
v1.0 - 19/04/2019 - KGosavi - Created  ( Code coverage 87% )
v1.1 - 7/5/2019 - Sneha U - Clear report fields for decouple
*****************************************************************/
public with sharing class CreditDeviceDecoupleController{
    public Boolean HasPermission {get;set;}
    public String RecordObjectName {get;set;}
    public Id RecordId{get;set;}
    public String CompanyId{get;set;}
    public Map <String,Schema.SObjectType> MapOfGlobalDescribeData = new Map <String,Schema.SObjectType>();
    private List<Account> listAcc = new List<Account>();
    private List<Lead> listLead = new List<Lead>();
    private Sobject obj;
    
    //Load the values in constructor which are required
    public CreditDeviceDecoupleController()
    {
        //check if use has permissionSet assigned or not.
        List<string> listPermissionSetNames = new List<String> {'CreditDevice_Admin','CreditDevice_User'};
        HasPermission = Util.hasPermission(listPermissionSetNames); //Check if the user has permission or not.
        if(!HasPermission)
        {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, System.Label.MSG_NO_ACCESS_TO_SECTION));
            return;
        }
        RecordId = ApexPages.currentPage().getParameters().get('id');
        RecordObjectName = RecordId.getSObjectType().getDescribe().getName();
        MapOfGlobalDescribeData = MonitoringUtility.MapOfGlobalDescribeData;
        obj = MapOfGlobalDescribeData.get(RecordObjectName).newSObject() ; //create instance of object dynamically
        obj.id = RecordId;
        if('Account'.EqualsIgnoreCase(RecordObjectName))
        {
            String accountQuery = 'SELECT Id,Name,CreditDevice__Company_Id__c,Is_International__c,Registration_Number__c,Registration_VAT_Number__c,BillingCity,BillingCountry,BillingPostalCode';
            accountQuery+= ' FROM ACCOUNT WHERE Id = :RecordId';
            listAcc =  Database.Query(accountQuery);                     
            if(!listAcc.isEmpty())
            {
                CompanyId = listAcc[0].CreditDevice__Company_Id__c;
            }
        }
        if('Lead'.EqualsIgnoreCase(RecordObjectName))
        {
            String leadQuery = 'SELECT Id,Name,CreditDevice__Company_Id__c,Is_International__c,Registration_Number__c,Registration_VAT_Number__c,City,Country,PostalCode';
            leadQuery += ' FROM LEAD WHERE Id = :RecordId';
            listLead =  Database.Query(leadQuery);
            if(!listLead.isEmpty())
            {
                CompanyId = listLead[0].CreditDevice__Company_Id__c;
            }
        }
        if(String.isBlank(CompanyId))
        {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, 'This record is not connected to CreditDevice System'));
            return;
        }
    }
/*
* Description: This method is used to decouple the record from creditDevice system.
*/
    public PageReference decoupleRecord()
    {
        try
        {
            List<String> listFieldNames = new List<String>();
            listFieldNames.add('CreditDevice__Company_Id__c');
            listFieldNames.add('CreditDevice__Is_International__c');
            Boolean hasUpdatePermission = Util.hasPermissions(obj,'Update',listFieldNames,new List<String>(), new List<String>());
            if(hasUpdatePermission)
            {
                obj.put('CreditDevice__Company_Id__c','');
                obj.put('CreditDevice__Is_International__c',false);

                //v1.1 Clear report fields for decouple
                obj.put('CreditDevice__Credit_Rating__c','');        
                obj.put('CreditDevice__Score__c', null);
                obj.put('CreditDevice__Limit_Advice__c', '');
                obj.put('CreditDevice__Payment_Score__c',null);
                obj.put('CreditDevice__Risk__c','');
                obj.put('CreditDevice__Credit_Report_Order_Date__c',null);
                obj.put('CreditDevice__InquiryId__c',null); 
                obj.put('CreditDevice__'+RecordObjectName+'_Monitoring__c',false);      
                obj.put('CreditDevice__Head_Quarter__c',false);  

                update obj;
                PageReference ref = redirectToRecord();
                return ref;
            }else
            {
                Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, System.Label.ERROR_NO_CD_CRUD_PERMISSION));
            }
            return null;
        }catch(Exception e)
        {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, e.getMessage()));
            return null;
        } 
    }
/*
* Description: This method is used to redirect to record detail page.
*/
    public PageReference redirectToRecord()
    {
        //Redirect to the last account created.
        PageReference pageRef = new ApexPages.StandardController(obj).view();
        pageRef.setRedirect(true);
        return pageRef;
        
    }
}