/***************************************************************
    Class : CreditDeviceCustomException
    Developer : HetalS
    Created Date : 04/12/19
    Summary : This class is used to throw custom exception
    Change Log: 
    v1.0 - 04/12/19 - HetalS - Created this new controller.(% Code Coverage)
*****************************************************************/
public with sharing class CreditDeviceCustomException extends Exception{
}