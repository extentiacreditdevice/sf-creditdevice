@isTest
public class TestAppConfigurationController{
    @testSetup static void setup() {
        Credit_Device_Custom_Setting__c creds = new Credit_Device_Custom_Setting__c();
        creds.Name = 'CreditDeviceCreds';
        creds.Username__c = 'Test';
        creds.Password__c ='Test@1234';
        creds.Endpoint_URL__c = 'https://inquiry.creditandcollection.nl/api';
        insert creds;
        list<CreditDevice__APIFields__c> apiFieldsList = new List<CreditDevice__APIFields__c>();
        CreditDevice__APIFields__c apifield = new CreditDevice__APIFields__c();
        apifield.Name = 'Id';
        apifield.Path__c = 'Id';
        apifield.Label__c = 'Id';
        apifield.CreditDevice__Order_Number__c =1;
        apifield.Is_International__c = true;
        apifield.Field_Type__c ='Boolean';
        apifield.Available_for_Contact__c = false;
        apiFieldsList.add(apifield);
        
        CreditDevice__APIFields__c apifield1 = new CreditDevice__APIFields__c();
        apifield1.Name = 'Name';
        apifield1.Path__c = 'contact_info/name';
        apifield1.Label__c = 'Name';
        apifield1.CreditDevice__Order_Number__c =1;
        apifield1.Field_Type__c ='String';
        apifield1.Available_for_Contact__c = false;
        apifield1.Is_International__c = false;
        apiFieldsList.add(apifield1);
        
        CreditDevice__APIFields__c apifield2 = new CreditDevice__APIFields__c();
        apifield2.Name = 'Country';
        apifield2.Path__c = 'contact_info/country';
        apifield2.Label__c = 'Country';
        apifield2.CreditDevice__Order_Number__c =2;
        apifield2.Field_Type__c ='String';
        apifield2.Available_for_Contact__c = false;
        apifield2.Is_International__c = false;
        apiFieldsList.add(apifield2);
        
        insert apiFieldsList;
        
        Credit_Device_General_Setting__c generalSetting = new Credit_Device_General_Setting__c();
        generalSetting.Name = 'GeneralSetting';
        generalSetting.Disable_Lead__c = false;
        generalSetting.Dutch_Country_Code__c = 'NL';
        generalSetting.Dutch_Language_Code__c ='NL';
        generalSetting.International_Product_Code__c = '701';
        generalSetting.National_Product_Code__c ='702';
        generalSetting.Non_Dutch_Language_Code__c ='EN';
        generalSetting.CreditDevice__Alert_Type_ALL__c ='INSOLVENCY,REVIEW_INCREASE,REVIEW_DECREASE,TERMINATION';
        generalSetting.CreditDevice__Alert_Type_HQ__c ='NAW';
        insert generalSetting;
        
        List<CreditDevice__Country__c> countryList = new List<CreditDevice__Country__c>();
        CreditDevice__Country__c country1 = new CreditDevice__Country__c();
        country1.CreditDevice__Country_Code__c ='NL';
        country1.Name = 'Netherlands';
        countryList.add(country1);
        
        CreditDevice__Country__c country2 = new CreditDevice__Country__c();
        country2.CreditDevice__Country_Code__c ='IN';
        country2.Name = 'India';
        countryList.add(country2);
        insert countryList;
        
        List<CreditDevice__FieldMapping__c> fieldMappingList = new List<CreditDevice__FieldMapping__c>();
        CreditDevice__FieldMapping__c fieldmapping1 = new CreditDevice__FieldMapping__c();
        fieldmapping1.Name ='Account-name';
        fieldmapping1.SF_Fields__c ='name';
        fieldmapping1.API_Fields__c = 'contact_info/name';
        fieldmapping1.Object__c = 'Account';
        fieldMappingList.add(fieldmapping1);
        insert fieldMappingList;
        
        List<CreditDevice__FieldMapping__c> fieldMappingListND = new List<CreditDevice__FieldMapping__c>();
        CreditDevice__FieldMapping__c fieldmapping2 = new CreditDevice__FieldMapping__c();
        fieldmapping2.Name ='LeadND-city';
        fieldmapping2.CreditDevice__International__c=true;
        fieldmapping2.SF_Fields__c ='city';
        fieldmapping2.API_Fields__c = 'city';
        fieldmapping2.Object__c = 'Lead';
        fieldMappingListND.add(fieldmapping2);
        insert fieldMappingListND;
    }
    static testmethod void TestDutchList(){
        Id FId = 'a010o00001uPf8bAAC';
        Boolean IsChecked= false;
        String SFName = 'Name';
        String APIName = 'Company_name';
        String selecteOBJ= 'Account'; 
        
        List<AppConfigurationController.FieldsMappingWrapper> wrapperList = new List<AppConfigurationController.FieldsMappingWrapper>();
        AppConfigurationController.FieldsMappingWrapper testWrap =new AppConfigurationController.FieldsMappingWrapper(FId, IsChecked, SFName,APIName,SFName,1);
        
        wrapperList.add(testWrap);
        
        AppConfigurationController appConfig=new AppConfigurationController();
       
        appconfig.SelectedObject = selecteOBJ;
        System.assertEquals('Account',appconfig.SelectedObject);
        appConfig.GetFieldsMappingWrapperList();
        appConfig.GetListOfObjects();
        appConfig.GetApiObjectFields(appconfig.SelectedObject);
        appConfig.GetSelectedObjectFields();
        appConfig.SaveMapping();
        appConfig.ValidateMapping(wrapperList);
    }
    
    static testmethod void TestNonDutchList(){
        Id FId1 = 'a010o00001vAxmjAAC';
        Boolean IsChecked1= false;
        String SFName1 = 'city';
        String APIName1 = 'city';
        String selecteOBJND='Lead';
        
        List<AppConfigurationController.FieldsMappingWrapperNDutch> wrapperNDutch = new List<AppConfigurationController.FieldsMappingWrapperNDutch>();
        AppConfigurationController.FieldsMappingWrapperNDutch testWrapNDutch=new AppConfigurationController.FieldsMappingWrapperNDutch(FId1, IsChecked1, SFName1,APIName1,SFName1,1);
        wrapperNDutch.add(testWrapNDutch);
        
        AppConfigurationController appConfigNDutch=new AppConfigurationController();
        appConfigNDutch.SelectedObjectNDutch = selecteOBJND;
        System.assertEquals('Lead',appConfigNDutch.SelectedObjectNDutch);
        appConfigNDutch.GetFieldsMappingWrapperListND();
        appConfigNDutch.GetListOfObjectsND();
        appConfigNDutch.GetApiObjectFieldsND(appConfigNDutch.SelectedObjectNDutch);
        appConfigNDutch.GetSelectedObjectFieldsND();
        appConfigNDutch.SaveMappingNDutch();
        appConfigNDutch.ValidateMappingNDutch(wrapperNDutch);
        
    }
    static testmethod void TestCredentials(){
        // appConfigCred.objSetting.Endpoint_URL__c= TestURL, appConfigCred.objSetting.Username__c = TestUName, appConfigCred.objSetting.Password__c =TestPWD
        AppConfigurationController appConfigCred=new AppConfigurationController();
        System.assertNotEquals(null,appConfigCred);
        appConfigCred.SaveCredentials();
    }
    static testmethod void Testmethod1(){
        AppConfigurationController appConfigController =new AppConfigurationController();
        CreditDevice__FieldMapping__c fieldMapping = [Select id from CreditDevice__FieldMapping__c where Name='Account-name' LIMIT 1];
        System.assertNotEquals(null,fieldMapping);
        appConfigController.ListFieldsMappingWrap[0].SFFieldName = '';
        appConfigController.ListFieldsMappingWrap[0].FieldMapId =fieldMapping.id;
        appConfigController.ValidateMappingNDutch(appConfigController.ListFieldsMappingWrapNDutch);
        appConfigController.ListFieldsMappingWrap.clear();
        appConfigController.ListFieldsMappingWrapNDutch.clear();
        appConfigController.SaveMapping();
        appConfigController.SaveMappingNDutch();
    }
    static testmethod void TestmethodGeneralSetting(){
        AppConfigurationController testGSController =new AppConfigurationController();
        testGSController.GeneralSetting =new Credit_Device_General_Setting__c();
        System.assertEquals(new Credit_Device_General_Setting__c(),testGSController.GeneralSetting);
    }
    
}