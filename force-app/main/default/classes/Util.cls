/***************************************************************
Class : Util
Developer : Hetal Shukla
Created Date : 06/03/2019
Details : The utility class which has been used for the generic code
Change Log : 
<1.0> - 03/06/2019 - HetalS - Created  ( Code coverage 92% )
<1.1> - 03/22/2019 - KalpanaG - Added permission set check
<1.2> - 04/12/2019 - HetalS - Added CRUD check for Object and fields
*****************************************************************/
public with sharing class Util
{     
    /****************************************************************
* @Description: This method is used to generate the api request based on the parameters passed
* @Returns: HttpRequest.
* @Params: Map<String,string> requestHeaderMap :  Map of key value to set in a header of the request
* @Params: String apiName                      :  Name of the API i.e. 'inquiries', 'instants', etc
* @Params: String requestBody                  :  Request body
* @Params: String method                       :  GET/POST
****************************************************************/
    public static HttpRequest GetGeneratedRequest(Map<String,string> requestHeaderMap, String apiName, String requestBody ,String method)
    {
        HttpRequest request = new HttpRequest();
        Credit_Device_Custom_Setting__c creds = Credit_Device_Custom_Setting__c.getValues('CreditDeviceCreds');
        if(creds==null || String.isBlank(creds.Username__c) || String.isBlank(creds.Password__c))
        {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, 'Please set the credentials properly'));
            return null;
        }else
        {
            string username = creds.Username__c; 
            string password = creds.Password__c;     
            string endpoint = creds.Endpoint_URL__c;
            endpoint +=apiName;              
            Blob headerValue = Blob.valueOf(username + ':' + password);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);            
            request.setHeader('Authorization', authorizationHeader);
            if(requestHeaderMap!= null)
            {
                for(string key:requestHeaderMap.keySet())
                {
                    request.setHeader(key, requestHeaderMap.get(key));
                }
            }
            if(requestBody != null)
            {
                request.setBody(requestBody);
            }
            request.setEndpoint(endpoint);
            request.setTimeout(120000);
            request.setMethod(method);                   
            return request;
        }
    }
    /****************************************************************
* @Description: This method is used to check if the current user has Permission or not.
* @Returns: True if user hs permission assigned else false.
****************************************************************/
    public static Boolean hasPermission( List<String> listpermissionsetNames)
    {
        Boolean hasPermisson = false;
        try
        {
            List<PermissionSetAssignment> listPermissionsetAssignment = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name IN :listpermissionsetNames  AND AssigneeId =:UserInfo.getUserId() LIMIT 1];
            if(listPermissionsetAssignment.size()>0)
            {
                hasPermisson = true;
            }
            return hasPermisson;
        }
        catch(Exception ex) 
        {
            system.debug('exception occurred==>  '+ex.getmessage());
            return false;
        }
        
    }
    /****************************************************************
* @Description: This method is used to get the list of all available countries countries.
* @Returns: List of countries
****************************************************************/
    public static List<SelectOption> getCountries()
    {
        List<SelectOption> listOptions = new List<SelectOption>();
        List<CreditDevice__Country__c> listCountry = CreditDevice__Country__c.getAll().values();
        if(!listCountry.isEmpty())
        {
            listCountry.sort();
            for(CreditDevice__Country__c countrydetails : listCountry)
            {
                listOptions.add(new selectoption(countrydetails.CreditDevice__Country_Code__c,countrydetails.Name));
            }
        }
        return listOptions;
    }
    /****************************************************************
* @Description: This method is used to get the map of duplicate accounts or leads.
* @Returns: Map of Account/Lead company id and record id.
****************************************************************/
    public static Map<String,Id> checkDuplicates( List<String> companyIdList)
    {
        //Check if duplicate account exist 
        List<Account> listAcc = [SELECT Id,CreditDevice__Company_Id__c FROM Account where CreditDevice__Company_Id__c IN : companyIdList];
        //Check if duplicate lead exist 
        List<Lead> listLead = [SELECT Id,CreditDevice__Company_Id__c FROM Lead where CreditDevice__Company_Id__c IN : companyIdList];
        Map<String,Id> mapCompanyIdToSfId = new Map<String,Id>();      
        if(!listAcc.isEmpty())
        {
            for(Account acc: listAcc)
            {
                mapCompanyIdToSfId.put(acc.CreditDevice__Company_Id__c,acc.id);
            }
        }
        if(!listLead.isEmpty())
        {
            for(Lead ld: listLead)
            {
                if(!mapCompanyIdToSfId.containsKey(ld.CreditDevice__Company_Id__c))
                {
                    mapCompanyIdToSfId.put(ld.CreditDevice__Company_Id__c,ld.id);
                }
            }
        }
        return mapCompanyIdToSfId;
    }
    /****************************************************************
* @Description: This method is used to get the map of all available countries with its code.
* @Returns: Map of country code and country name
****************************************************************/
    public static Map<String,String> getCountryCodeMap()
    {
        Map<String,String> mapCountryCodeName = new Map<String,String>();
        List<CreditDevice__Country__c> listCountry = CreditDevice__Country__c.getAll().values();
        if(!listCountry.isEmpty())
        {           
            for(CreditDevice__Country__c countrydetails : listCountry)
            {
                mapCountryCodeName.put(countrydetails.CreditDevice__Country_Code__c,countrydetails.Name);
            }
        }
        return mapCountryCodeName;
    }
    /* 
* Description  To get the fields for which the logged in user has access 
*@param   String        SObject
*@param   String         CRUD operation
*@param   List<String>   Accessible fields of object
*/
    public static Map<String,String> getAccessibleFields(SObject sObj, String operationType, Map<String,String> MapOfSftoApifields)
    {
        List <String> listOfAccessibleFieldsName = new List <String> ();
        Map<String,String> MapOfSfFieldstoApFfields = new Map<String,String>();
        List <String> listOfNoPermissionFieldsName = new List <String> ();
        List <String> listFieldApiNames = new List<String>();
        listFieldApiNames.addAll(MapOfSftoApifields.keySet());
        if(sObj != null)
        {         
            hasPermissions(sObj, operationType,listFieldApiNames , listOfNoPermissionFieldsName, listOfAccessibleFieldsName);                  
        }
        if(!listOfAccessibleFieldsName.isEmpty())
        {
            for(String fieldName : listOfAccessibleFieldsName)
            {
                MapOfSfFieldstoApFfields.put(fieldName,MapOfSftoApifields.get(fieldName));
            }
        }
        return MapOfSfFieldstoApFfields;  
    }
    /* 
* Description  Verify the current user has create/update/view/upsert/delete permission of the object and throws the error is the user does not have access
*@param   String        SObject
*@param   String         CRUD operation
*@param   List<String>   fields of object
*/
    public static void checkPermissions(SObject sObj, String operationType, List <String> listFieldApiNames)
    {
        if(sObj != null) 
        {
            List <String> listOfNoPermissionFieldsName = new List <String> ();
            List <String> listOfAccessibleFieldsName = new List <String> ();
            if(!hasPermissions(sObj, operationType, listFieldApiNames, listOfNoPermissionFieldsName, listOfAccessibleFieldsName)) 
            {
                String noPermissionFieldsNames = '';
                noPermissionFieldsNames = string.join(listOfNoPermissionFieldsName,',');
                String[] arguments = new String[] {operationType, sObj.getSObjectType().getDescribe().getLabel() + ' [' + noPermissionFieldsNames + '] '};
                    throw new CreditDeviceCustomException(String.format(System.Label.ERROR_NO_CD_CRUD_PERMISSION, arguments));
            }
        }else 
        {
            String[] arguments = new String[] {operationType, sObj.getSObjectType().getDescribe().getLabel()};
                throw new CreditDeviceCustomException(String.format(System.Label.ERROR_NO_CD_CRUD_PERMISSION, arguments));
        }
    }
    public static Boolean hasPermissions(SObject sObj, String operationType, List <String> listFieldNames, List <String> listOfNoPermissionFieldsName, List<String> listOfAccessibleFieldsName)
    {
        Boolean hasPermission = false;
        Schema.sObjectType objType = sObj.getSObjectType();
        Schema.DescribeSObjectResult objResult = objType.getDescribe();
        Map <String, Schema.SObjectField> mapFieldInfo = objResult.fields.getMap();
        if (('Read'.EqualsIgnoreCase(operationType) && objResult.isAccessible()) || 
            ('Insert'.EqualsIgnoreCase(operationType) && objResult.isCreateable()) || 
            ('Update'.EqualsIgnoreCase(operationType) && objResult.isUpdateable()) || 
            ('Upsert'.EqualsIgnoreCase(operationType) && objResult.isCreateable() && objResult.isUpdateable())) 
        {
            hasPermission = true;
            for (String fieldName: listFieldNames) 
            {   
                Schema.SObjectField tmpfield = mapFieldInfo.get(fieldName.toLowerCase());
                if (tmpfield != null) 
                {
                    if (!(mapFieldInfo.get(fieldName.toLowerCase()).getDescribe().isAccessible())) 
                    {
                        hasPermission = false;
                        listOfNoPermissionFieldsName.add(tmpfield.getDescribe().getLabel());                            
                    }else
                    {
                        listOfAccessibleFieldsName.add(fieldName);//Accessible fields
                    }
                }else 
                { //Invalid field name                        
                    hasPermission = false;
                    listOfNoPermissionFieldsName.add(fieldName + '*'); //Wrong field name                       
                }
            }
        } 
        else if ('Delete'.EqualsIgnoreCase(operationType) && objResult.isDeletable())
        {
            hasPermission = true;
        }
        
        return hasPermission;
    }
    /*Description: To send the email from batch class
    *@param: AsyncApexJob asyn
    *@param: String emailBody 
    */
    public static void SendEmailToAdminUser(AsyncApexJob asyn, String emailBody, String subject){
        try
        {   
            emailBody = (String.isNotBlank(emailBody) ? emailBody : ('Hi '+asyn.CreatedBy.Name+'\n\n'+'The batch apex job '+ asyn.Id+' is processed. \n\n'+ 'Batch Status: ' + asyn.Status+'\n'+'No. of records processed: '+ asyn.TotalJobItems));
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if(!String.isBlank(subject))
            {
                mail.setSubject(subject);
            }else
            {
                mail.setSubject('Monitoring Batch Report ' + asyn.Status);
            }
            mail.setSaveAsActivity(false);            
            mail.setTargetObjectId(asyn.CreatedById);
            mail.setPlainTextBody(emailBody);
            if(!Test.isRunningTest())
            {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
            }            
        }
        catch(Exception e)
        {
            System.debug(LoggingLevel.Error,'Exception in Sending Mobitoring Batch Email>>'+e.getStackTraceString());
            System.debug(LoggingLevel.Error,e.getMessage());
        }
    }
}