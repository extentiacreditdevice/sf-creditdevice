({ 
    fetchInquiryCallCount:0,
    /*
    If inquiry is finished, it will get the updated report data, else will fetch the inquiry again using the last inquiry generated
    */
    FetchInquiry: function(component,event,helper,result)
    {
        try
        {
            if(!$A.util.isEmpty(result) && result.Success && !$A.util.isEmpty(result.DataMap) && !$A.util.isEmpty(result.DataMap.inquiryResponse))
            {
                if(result.DataMap.inquiryResponse.item && result.DataMap.inquiryResponse.item.status == 'pending')
                {                    
                    helper.fetchInquiryCallCount++;                    
                    if(helper.fetchInquiryCallCount > component.get('v.fetchInquiryRetryCount'))
                    {                        
                        component.set('v.message','The report could not be generated. Please contact the CreditDevice.');
                        helper.hideSpinner(component, event, helper); 
                    }
                    else
                    {
                        var timeDelay = (component.get("v.fetchInquiryCallTimeDelay") ? component.get("v.fetchInquiryCallTimeDelay") : 5000);
                        helper.showSpinner(component, event, helper);            
                        window.setTimeout(
                            $A.getCallback(function() 
                            {               
                                var request = {};       
                                var utilityCmp = component.find('utilityCmp');  
                                var inquiryId = result.DataMap.inquiryResponse.item.id;
                                utilityCmp.CallApexMethod(component, 'c.FetchPendingInquiry' ,{inquiryId:inquiryId, recordId:component.get("v.recordId")}, function(result) {
                                    
                                    if(result.Success){
                                        if(result.DataMap.inquiryResponse && result.DataMap.inquiryResponse.item && result.DataMap.inquiryResponse.item.status == 'pending')
                                        {                                     
                                              helper.FetchInquiry(component,event,helper,result);
                                        }
                                        else if(result.DataMap.inquiryResponse && result.DataMap.inquiryResponse.item && result.DataMap.inquiryResponse.item.status != 'finished'){
                                            component.set('v.message','The report could not be generated.');
                                            helper.hideSpinner(component, event, helper); 
                                        }
                                        else{                       
                                           helper.getUpdatedData(component,event,helper);
                                        }
                                    }
                                    else
                                    {
                                        helper.showMessage(component, event, helper,result.MessageList); 
                                        helper.hideSpinner(component, event, helper); 
                                    }        
                                });              
                        }), timeDelay);
                    }                   
                }
                else if(result.DataMap.inquiryResponse.item && result.DataMap.inquiryResponse.item.status != 'finished')
                {
                    component.set('v.message','The report could not be generated.');
                    helper.hideSpinner(component, event, helper); 
                }
                else
                {                    
                    helper.getUpdatedData(component,event,helper);
                }
            }
            else
            { 
                helper.showMessage(component, event, helper,result.MessageList);          
                helper.hideSpinner(component, event, helper); 
            }
        }  
        catch(e)
        {
            component.set('v.message',e);
            helper.hideSpinner(component, event, helper); 
        }
        
    },
    /*
    Get the updated report data from Account/Lead
    */
    getUpdatedData :function(component,event,helper)
    {
        try
        {
            var accountData = component.get('v.accountData');
            if(accountData)
            {
                accountData = JSON.parse(JSON.stringify(accountData));
            }             
           
            var utilityCmp = component.find('utilityCmp');  
            helper.showSpinner(component, event, helper); 
            utilityCmp.CallApexMethod(component, 'c.GetUpdatedReportData' ,{recordId:component.get("v.recordId")}, function(result) {
                if(result.Success && result.DataMap.data)
                {
                    var companyCreditDate ='';
                    if(accountData)
                    {
                        accountData.creditLimitValue = result.DataMap.data.creditLimit;
                        accountData.creditRatingValue = result.DataMap.data.creditRating;
                        accountData.orderDateValue = result.DataMap.data.orderDate;
                        accountData.paymentScoreValue = result.DataMap.data.paymentScore;
                        accountData.reportLink = result.DataMap.data.reportLink;
                        accountData.riskValue = result.DataMap.data.risk;
                        accountData.scoreValue = result.DataMap.data.score;
                        accountData.hasCreditLimitChange = result.DataMap.data.hasCreditLimitChange;
                        if(result.DataMap.data.orderDate!='' && result.DataMap.data.orderDate!=null){
                            var cDate = new Date(result.DataMap.data.orderDate);
                            var monthCDate = cDate.getMonth()+1;
                            companyCreditDate = cDate.getFullYear()+'-'+monthCDate+'-'+cDate.getDate();
                        }
                        
                    }                  
                    component.set('v.accountData',accountData);  
                    component.set('v.companyCreditDate',companyCreditDate);                      
                }
                else
                {
                    helper.showMessage(component, event, helper,result.MessageList); 
                }
                helper.hideSpinner(component, event, helper);  
                helper.fetchInquiryCallCount = 0;          
            });
        }
        catch(e){
            component.set('v.message',e);
        }
        
    },
    showSpinner:function(component,event,helper)
    {
        component.set('v.Spinner',true);
    },
    hideSpinner:function(component,event,helper)
    {
        component.set('v.Spinner',false); 
    },
    /*To display the error message*/
    showMessage:function(component,event,helper, messageList)
    {
        var messageToDisplay = ' ';
        if(messageList.length > 0)
        {
            for(var i=0;i<messageList.length;i++)
            {
                messageToDisplay+=messageList[i].message;
            }                
        }
        component.set('v.message',messageToDisplay);
    },
    /*To formate the Date in the following formate: Mon Date,Year for e.g. Mar 19, 2019 */
    formateOrderDate:function(component,helper)
    {
        try
        {
             var accountData = component.get('v.accountData');
            if(accountData)
            {
                accountData = JSON.parse(JSON.stringify(accountData));
                var dateString = accountData.orderDateValue;      
                if(dateString)
                {
                   component.set("v.accountData.orderDateValue", $A.localizationService.formatDate(dateString)); 
                }      
                
            }
        }
        catch(e)
        {
            console.log(e);
        }
    }
})