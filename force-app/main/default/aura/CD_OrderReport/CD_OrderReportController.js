({ 
    onInit:function(component, event, helper)
    {           
        try
        {
            var utilityCmp = component.find('utilityCmp');  
            utilityCmp.CallApexMethod(component, 'c.GetCustomSetting' ,{}, function(result) {                            
                if(result && result.Success)
                {
                    if (result.DataMap.settings != null) 
                    {
                        component.set('v.fetchInquiryRetryCount',result.DataMap.settings.CreditDevice__FetchInquiryRetryCount__c);
                        component.set('v.fetchInquiryCallTimeDelay',result.DataMap.settings.CreditDevice__FetchInquiryCallTimeDelay__c);                        
                    }                    
                }              
            });
        }   
        catch(e)
        {
            component.set('v.message',e);
        }   
    },
    /*
    Place inquiry and get the report
    */
    getReportData : function(component, event, helper)
    {
        try
        {
            helper.showSpinner(component, event, helper);
            var request = {};       
            var utilityCmp = component.find('utilityCmp');  
            utilityCmp.CallApexMethod(component, 'c.PlaceandGetReportData' ,{recordId:component.get("v.recordId")}, function(resultStringified) {
                let result = JSON.parse(resultStringified);
                helper.hideSpinner(component, event, helper);
                if(result && result.Success)
                {
                    helper.FetchInquiry(component, event, helper,result);
                }
                else
                {
                    helper.showMessage(component, event, helper,result.MessageList);
                }                
            });
        }
        catch(e)
        {
            component.set('v.message',e);
        }       
    }
    
})