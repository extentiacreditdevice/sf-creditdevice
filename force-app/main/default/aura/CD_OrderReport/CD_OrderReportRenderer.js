({
	rerender : function(component, helper)
  {
      	this.superRerender(); 
      	var accountData = component.get('v.accountData');        
        if(accountData)
        {
            accountData = JSON.parse(JSON.stringify(accountData));
            var hasCreditLimitChange = accountData.hasCreditLimitChange;
            component.set('v.hasCreditLimitChange',hasCreditLimitChange);
            helper.formateOrderDate(component, helper);
        }     
    }
})