({
	callApexMethod: function (callerComponent, controllerMethod, request, callback) {
        var response = {};
        var action = callerComponent.get(controllerMethod);

        try{            
          if(request){              
                var req  = JSON.parse(JSON.stringify(request));
                if(req && req.request){
                   request = {'request': JSON.stringify(req.request)} ;                   
                   action.setParams(request);
                }
                else{
                    action.setParams(request);
                }
                
            }
              action.setCallback(this, function(serverResponse) {
                var state = serverResponse.getState();    
                if (state === "SUCCESS") {                    
                    response = serverResponse.getReturnValue();
                    
                } else {
                    response.messages = serverResponse.getError();                  
                }
                 callback(response);
               
            });
           
        }
        catch(e){
            response.messages = e;
            callback(response);
            
        }
         $A.enqueueAction(action);
    },
})