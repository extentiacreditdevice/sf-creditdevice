({
	callApexMethod: function (component, event, helper) {
        var params = event.getParam('arguments');
        var component, callback, controllerMethod,request ;
        if (params) {
            component =  params.component;
            callback = params.callback;
            controllerMethod = params.controllerMethod;
            request = params.request;
        } 
        helper.callApexMethod(component, controllerMethod,request,callback);  
    },
})